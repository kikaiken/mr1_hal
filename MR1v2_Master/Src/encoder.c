/*
 * encoder.cpp
 *
 *  Created on: 2018/05/23
 *      Author: Ryohei
 */


#include "encoder.h"
#include <stdio.h>
#include <math.h>

//エンコーダ位置の定数定義
//X方向エンコーダ
#define ENC_POS_X1		0.239f
#define ENC_POS_Y1		0.020f
//Y方向エンコーダ
#define ENC_POS_X2		0.179f
#define ENC_POS_Y2		0.041f

//static関数プロトタイプ宣言
static float EncoderCount2Meter(int32_t);

/*
 * エンコーダ値からロボットの速さを求める。オドメトリも求める。
 * @param
 * @return
 * @note	CTRL_CYCLE(s)毎に呼び出すこと。
 */
void EncoderGetData(struct_state *state) {
	static int32_t enc_x_prev_cnt = ENC_CNT_RESET, enc_y_prev_cnt = ENC_CNT_RESET;
	int32_t enc_x_present_cnt = __HAL_TIM_GET_COUNTER(&ENC_X_TIM_HANDLER);
	int32_t enc_y_present_cnt = __HAL_TIM_GET_COUNTER(&ENC_Y_TIM_HANDLER);
	volatile float dif_x, dif_y;

#ifdef ENC_X_FW_CNTUP
	dif_x = EncoderCount2Meter(enc_x_present_cnt - enc_x_prev_cnt) - (-state->angvel * CTRL_CYCLE * ENC_POS_Y1);
#endif
#ifdef ENC_X_FW_CNTDOWN
	dif_x = EncoderCount2Meter(enc_x_prev_cnt - enc_x_present_cnt) - (-state->angvel * CTRL_CYCLE * ENC_POS_Y1);
#endif

#ifdef ENC_Y_FW_CNTUP
	dif_y = EncoderCount2Meter(enc_y_present_cnt - enc_y_prev_cnt) - (state->angvel * CTRL_CYCLE * ENC_POS_X2);
#endif
#ifdef ENC_Y_FW_CNTDOWN
	dif_y = EncoderCount2Meter(enc_y_prev_cnt - enc_y_present_cnt) - (state->angvel * CTRL_CYCLE * ENC_POS_X2);
#endif

	//a.過去データ格納
	state->prev_vel = state->vel;
	state->prev_odometry = state->odometry;

	//速度の計算
	state->vel.vx = (dif_x * cosf(state->odometry.theta) - dif_y * sinf(state->odometry.theta)) / CTRL_CYCLE;
	state->vel.vy = (dif_x * sinf(state->odometry.theta) + dif_y * cosf(state->odometry.theta)) / CTRL_CYCLE;
	state->vel.vabs = hypotf(state->vel.vx, state->vel.vy);

	//b.オドメトリの計算
	state->enc_odom.x += dif_x * cosf(state->odometry.theta) - dif_y * sinf(state->odometry.theta);
	state->enc_odom.y += dif_x * sinf(state->odometry.theta) + dif_y * cosf(state->odometry.theta);

	//d.カウント値保存
	enc_x_prev_cnt = enc_x_present_cnt;
	enc_y_prev_cnt = enc_y_present_cnt;
	//e.カウント値リセット
	if ((enc_x_present_cnt > 60035) || (enc_x_present_cnt < 5000)) {	//X方向エンコーダカウント値がオーバーフローしそうになったらリセット
		__HAL_TIM_SET_COUNTER(&ENC_X_TIM_HANDLER, ENC_CNT_RESET);
		enc_x_prev_cnt = ENC_CNT_RESET;
	}
	if ((enc_y_present_cnt > 60035) || (enc_y_present_cnt < 5000)) {	//Y方向エンコーダカウント値がオーバーフローしそうになったらリセット
		__HAL_TIM_SET_COUNTER(&ENC_Y_TIM_HANDLER, ENC_CNT_RESET);
		enc_y_prev_cnt = ENC_CNT_RESET;
	}
}

/*
 * エンコーダカウント値をメートルに変換
 * @param	enc_cnt : エンコーダのカウント値
 * @return	長さ(m)
 */
static float EncoderCount2Meter(int32_t enc_cnt) {
	return (float)((float)(enc_cnt * M_PI * (float)ENC_OMNI_RADIUS) / (float)ENC_RESOLUTION);
}

/*
 * エンコーダカウントスタート
 * @param
 * @return
 */
void EncoderEnable(void) {
	printf("Odom Encoder : Enable\n");
	HAL_TIM_Encoder_Start(&ENC_X_TIM_HANDLER, TIM_CHANNEL_ALL);
	HAL_TIM_Encoder_Start(&ENC_Y_TIM_HANDLER, TIM_CHANNEL_ALL);
	TIMx_ENC_X->CNT = ENC_CNT_RESET;
	TIMx_ENC_Y->CNT = ENC_CNT_RESET;
}

/*
 * エンコーダカウントストップ
 * @param
 * @return
 */
void EncoderDisable(void) {
	printf("Odom Encoder : Disable\n");
	HAL_TIM_Encoder_Stop(&ENC_X_TIM_HANDLER, TIM_CHANNEL_ALL);
	HAL_TIM_Encoder_Stop(&ENC_Y_TIM_HANDLER, TIM_CHANNEL_ALL);
	TIMx_ENC_X->CNT = ENC_CNT_RESET;
	TIMx_ENC_Y->CNT = ENC_CNT_RESET;
}


/*
 * start AngEncorder
 * @param
 * @return
 */
void AngleEncoderEnable(void) {
	__HAL_TIM_SET_COUNTER(&ENC_ANG_HANDLER, 5000);
	HAL_TIM_Encoder_Start(&ENC_ANG_HANDLER, TIM_CHANNEL_ALL);
}

/*
 * エンコーダ値からランチャーの角度を検出
 * @param
 * @return
 * @note	CTRL_CYCLE(s)毎に呼び出すこと。
 */
void AngleEncoderGet(int32_t *angpos) {

	//calc position
#ifdef ENC_ANG_FW_CNTUP
	*angpos = __HAL_TIM_GET_COUNTER(&ENC_ANG_HANDLER);
#endif
#ifdef ENC_ANG_FW_CNTDOWN
	*angpos = -__HAL_TIM_GET_COUNTER(&ENC_ANG_HANDLER);
#endif

}

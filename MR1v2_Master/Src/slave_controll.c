/*
 * slave_controll.c
 *
 *  Created on: 2019/03/25
 *      Author: naoki
 */
#include "main.h"
#include "slave_controll.h"

void slave_com_send(uint8_t tag, uint32_t val){
	handler_arg motor_val;
	motor_val.u32_val = val;
	uart_com_send(tag, motor_val);
}

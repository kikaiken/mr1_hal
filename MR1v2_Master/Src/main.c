/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "lwip.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "motor.h"
#include "mpu9250.h"
#include "periph_handler.h"
#include "user_interface.h"
#include "controller.h"
#include "robotstate.h"
#include "encoder.h"
#include "mode.h"
#include "so1602a_i2c.h"
#include "failsafe.h"
#include "light_math.h"
#include "path.h"
#include "slave_controll.h"
#include "dma_printf.h"
#include "rosbridge_proxy.h"
#include "controller/controller_com.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

//general flag
typedef enum orderstate{
	NO_FLAG,
	GET_FLAG,
	WAIT_NEXT
}FlagStateTypeDef;

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

//printf setting
#ifdef __GNUC__
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
void __io_putchar(uint8_t ch) {
	dma_printf_putc(ch);
}

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

#define TIM_MELODY		TIM5
#define TIM_CTRL		TIM6
#define TIM_10MS		TIM7
#define TIM_50MS		TIM11
#define TIM_UI			TIM12
#define TIM_ODOMETRY	TIM13
#define TIM_CTRL_HANDLER		htim6
#define TIM_10MS_HANDLER		htim7
#define TIM_50MS_HANDLER		htim11
#define TIM_UI_HANDLER			htim12
#define TIM_ODOMETRY_HANDLER	htim13

#define PATH_AFTERBRIDGE 850
#define PATH_AFTERLINE1  1382//1297

#define POS_THROWING_READY	8.48f
#define POS_THROWING		5.50f

#define PS_Btn(__BUTTON__) (button_data&(__BUTTON__))
#define PS_BtnChg(__BUTTON__) (!(pre_button_data&(__BUTTON__)) && (button_data&(__BUTTON__)))

//#define YABUSAME

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc3;
DMA_HandleTypeDef hdma_adc3;

I2C_HandleTypeDef hi2c2;

SPI_HandleTypeDef hspi3;

TIM_HandleTypeDef htim1;
TIM_HandleTypeDef htim2;
TIM_HandleTypeDef htim3;
TIM_HandleTypeDef htim4;
TIM_HandleTypeDef htim5;
TIM_HandleTypeDef htim6;
TIM_HandleTypeDef htim7;
TIM_HandleTypeDef htim8;
TIM_HandleTypeDef htim11;
TIM_HandleTypeDef htim12;
TIM_HandleTypeDef htim13;
TIM_HandleTypeDef htim14;

UART_HandleTypeDef huart1;
UART_HandleTypeDef huart2;
UART_HandleTypeDef huart3;
DMA_HandleTypeDef hdma_usart1_rx;
DMA_HandleTypeDef hdma_usart1_tx;
DMA_HandleTypeDef hdma_usart2_rx;
DMA_HandleTypeDef hdma_usart2_tx;
DMA_HandleTypeDef hdma_usart3_rx;
DMA_HandleTypeDef hdma_usart3_tx;

/* USER CODE BEGIN PV */
switchnum_t lchika_pin = SW_START;

struct_state state;			//current robot state

failsafe_t failsafe = NO_ERROR;

//flag
FlagStateTypeDef axisdirflag = NO_FLAG;
FlagStateTypeDef manual_flag = NO_FLAG;
FlagStateTypeDef maxspeedflag = NO_FLAG;
FlagStateTypeDef s_cmdflag = NO_FLAG;
FlagStateTypeDef shagaiautoflag = NO_FLAG;
FlagStateTypeDef LCD_flag = NO_FLAG;

FlagStateTypeDef bridgeflag = NO_FLAG;
FlagStateTypeDef line1_flag = NO_FLAG;
FlagStateTypeDef wallx_flag = NO_FLAG;
FlagStateTypeDef wally_flag = NO_FLAG;
FlagStateTypeDef throwflag = NO_FLAG;
FlagStateTypeDef f1_flag = NO_FLAG;
FlagStateTypeDef pram_flag = NO_FLAG;
FlagStateTypeDef melotest_flag = NO_FLAG;
FlagStateTypeDef lidardiff_flag = NO_FLAG;

uint8_t slavecom_cmd = State_Reset;

ToggleState sw_field = FIELD_RED;
ToggleState pre_sw_field=-1;
ToggleState sw_pilot;
ToggleState pre_sw_pilot=-1;
ToggleState sw_ctrl;
ToggleState sw_lidar;

//controller dir of axis
float axis_dir = -1;

uint32_t adc_data[2] = {0};	//2ch
uint32_t dyna_angle;
int32_t rail_angle;

//mode
PhaseAndMode current_motion;	//current motion

//UART Receive
uint8_t Uart_Buf[6]="0000\n\r";
uint8_t uart_flag=0;

vib_e vib_state = VIB_SMALL_MOTOR_OFF;

//シャガイを投げた個数
uint8_t num_of_shagai_thrown= 0;
int32_t shagai_throw_state = 0, shagai_throw_time = 0;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_DMA_Init(void);
static void MX_USART3_UART_Init(void);
static void MX_TIM1_Init(void);
static void MX_TIM3_Init(void);
static void MX_TIM2_Init(void);
static void MX_SPI3_Init(void);
static void MX_I2C2_Init(void);
static void MX_TIM6_Init(void);
static void MX_TIM8_Init(void);
static void MX_TIM4_Init(void);
static void MX_TIM7_Init(void);
static void MX_ADC3_Init(void);
static void MX_TIM14_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_TIM5_Init(void);
static void MX_TIM12_Init(void);
static void MX_TIM13_Init(void);
static void MX_TIM11_Init(void);
static void MX_USART2_UART_Init(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	static uint16_t pre_button_data = 0;
	static uint16_t button_data = 0;
	static analog_stick_f analog_left, analog_right;
	static float ps_speed_max = 1.0f;
	Path_StatusTypedef follow_states;
	static uint8_t ledstate;

	//shagai angle etc.
	if (htim->Instance == TIM_50MS){
		dyna_angle = adc_data[0] *  70 / 4096 +   170;
		rail_angle = adc_data[1] * 100 / 4096 + (-105);
		slave_com_send(Adjust_DynaAngle, dyna_angle);//center:215
		slave_com_send(Adjust_RailAngle, rail_angle);//-152 ~ -5
		LCD_flag = GET_FLAG;
	}

	//Lchika
	if (htim->Instance == TIM_UI) {
		if(lchika_pin == SW_START){
			HAL_GPIO_TogglePin(STARTLD_GPIO_Port,STARTLD_Pin);
		}
		else if(lchika_pin == SW_F1){
			HAL_GPIO_TogglePin(F1LD_GPIO_Port,F1LD_Pin);
		}
	}

	//Melody
	if (htim->Instance == TIM_MELODY) {
		UILEDSet(ledstate ^= 0b1111);
		Melody_Update();
	}

	//odometry timer to avoid failsafe
	if (htim->Instance == TIM_ODOMETRY) {
		//Get Angular Velocity
		MPU9250GetAngVel(&state);

		//Get RotaryEncoder Data
		EncoderGetData(&state);
	}

	//10msTimer
	if (htim->Instance == TIM_CTRL) {
		static int32_t time = 0;
		static struct_pos ref_pos;
		static struct_pos apply;
		static float odom_theta_buf;
		static int32_t shagai_pickup_state, shagai_pickup_time, ready_rotate_state;
		int32_t nearest_path = 0;

		//controller failsafe
		if (PS_Btn(CONTROLLER_L2) && PS_Btn(CONTROLLER_R2)) {
			failsafe |= CANCEL_PROCESS;
		}

		state.odometry.x = state.enc_odom.x;
		state.odometry.y = state.enc_odom.y;

	    if(sw_lidar == LIDAR_ON && current_motion != MODE_LIDAR_SETUP && current_motion != PHASE_GERGE && current_motion != PHASE_SHAGAI1_PICKUP){
	    	float diff_x_abs = fabsf(state.odometry.x - state.lidar_odom.x);
	    	float diff_y_abs = fabsf(state.odometry.y - state.lidar_odom.y);
	    	//許容誤差内
	    	if(diff_x_abs < 0.20f && diff_y_abs < 0.20f) {
	 	       state.odometry.x = state.enc_odom.x * 0.75f + state.lidar_odom.x * 0.25f;
	 	       state.odometry.y = state.enc_odom.y * 0.75f + state.lidar_odom.y * 0.25f;
	 	       if(lidardiff_flag == GET_FLAG){
		 	       BuzzerOff();
		 	       lidardiff_flag = NO_FLAG;
	 	       }
	    	}
	    	//許容誤差外
	    	else{
	    		if(lidardiff_flag == NO_FLAG){
		    		BuzzerOn(BUZZER_C8);
		    		lidardiff_flag = GET_FLAG;
	    		}
	    	}
	    }
		//MTに移行
		if (PS_BtnChg(CONTROLLER_DOWN)) {
			if(manual_flag == NO_FLAG){
				current_motion = MODE_MT;
				axis_dir = -1;
				Melody_Start(5);
				manual_flag = WAIT_NEXT;
			}
		}
		else if (PS_BtnChg(CONTROLLER_CROSS)) {
			if(manual_flag == NO_FLAG){
				current_motion = MODE_MT;
				axis_dir = -1;
				if(slavecom_cmd == State_Freeze || slavecom_cmd == State_LockShagai){
					slavecom_cmd = State_ExtendPoker;
					slave_com_send(slavecom_cmd, 0);
				}
				else if(slavecom_cmd >= State_ReadyToGrasp){
					slavecom_cmd = State_Freeze;
					slave_com_send(slavecom_cmd, 0);
				}
				manual_flag = WAIT_NEXT;
			}
		}
		else if(manual_flag == WAIT_NEXT){
			manual_flag = NO_FLAG;
		}

		//mode select
		switch(current_motion){
			//phase0:remote control
			case MODE_MT:	//手動操作
				//四角ボタンで操作方向を反転する
				if (PS_BtnChg(CONTROLLER_SQUARE)) {
					if(axisdirflag == NO_FLAG){
						axis_dir *= -1;
						axisdirflag = WAIT_NEXT;
					}
				}
				else if(axisdirflag == WAIT_NEXT){
					axisdirflag = NO_FLAG;
				}

				//左スティック押し込み時は最大速度を上げる
				if (PS_Btn(CONTROLLER_LSTICK)) {
					ps_speed_max = 1.5;
				} else {
					ps_speed_max = 0.75;
				}

				//並進
				ref_pos.x = ps_speed_max * analog_left.r * sinf(analog_left.theta) * axis_dir;
				ref_pos.y = ps_speed_max * analog_left.r * cosf(analog_left.theta) * axis_dir;

				//ps_speedを徐々に上げ下げする
				if (apply.x < ref_pos.x) {
					apply.x = fminf(apply.x + 3.0 * CTRL_CYCLE, ref_pos.x);
				} else if (apply.x > ref_pos.x) {
					apply.x = fmaxf(apply.x - 3.0 * CTRL_CYCLE, ref_pos.x);
				}
				if (apply.y < ref_pos.y) {
					apply.y = fminf(apply.y + 3.0 * CTRL_CYCLE, ref_pos.y);
				} else if (apply.y > ref_pos.y) {
					apply.y = fmaxf(apply.y - 3.0 * CTRL_CYCLE, ref_pos.y);
				}

				//回転
				ref_pos.theta += 0.8 * CTRL_CYCLE * (-analog_right.x);
				if (PS_Btn(CONTROLLER_L1)) {
					ref_pos.theta += 1.5 * CTRL_CYCLE;
				} else if (PS_Btn(CONTROLLER_R1)) {
					ref_pos.theta -= 1.5 * CTRL_CYCLE;
				}
				apply.theta = ref_pos.theta;

				failsafe |= UnderMotor_Velocity_withPID(&state, &apply);

				//slave cmd send
				if (PS_BtnChg(CONTROLLER_RSTICK)) {
					if(s_cmdflag == NO_FLAG){
						if(slavecom_cmd == State_Forest)	slavecom_cmd+=2;
						else								slavecom_cmd++;
						//一連終わったらShagai Resetに
						if(slavecom_cmd > SLV_CMD_MAX)		slavecom_cmd=State_ExtendPoker;
						slave_com_send(MIN(slavecom_cmd, SLV_CMD_MAX), 0);
						s_cmdflag = WAIT_NEXT;
					}
				}
				//Complete Reset
				else if (PS_BtnChg(CONTROLLER_TRIANGLE)) {
					if(s_cmdflag == NO_FLAG){
						slavecom_cmd = State_Reset;
						slave_com_send(slavecom_cmd, sw_field);
						s_cmdflag = WAIT_NEXT;
					}
				}
				//Shagai Reset
				else if (PS_BtnChg(CONTROLLER_CIRCLE)) {
					if(s_cmdflag == NO_FLAG){
						slavecom_cmd = State_AfterGerge;
						slave_com_send(slavecom_cmd, 0);
						s_cmdflag = WAIT_NEXT;
					}
				}
				else if(s_cmdflag == WAIT_NEXT){
					s_cmdflag = NO_FLAG;
				}

				//手動中に上ボタンで投げに行く動作を自動経路追従
				if (PS_BtnChg(CONTROLLER_UP) && (slavecom_cmd >= State_StorePoker)) {
					if (shagaiautoflag == NO_FLAG) {
						if (hypotf((((sw_field == FIELD_RED) ? (state.odometry.x) : (-state.odometry.x)) - SHAGAI1_PICKUP_X), (state.odometry.y - SHAGAI1_PICKUP_Y)) < 0.50f) {
							current_motion = PRE_PHASE_SHAGAI1_READY;
						} else if ((hypotf((((sw_field == FIELD_RED) ? (state.odometry.x) : (-state.odometry.x)) - THROWING_ZONE_ENTRANCE_X_23), (state.odometry.y - THROWING_ZONE_ENTRANCE_Y_23)) < 3.34f)
								&& (state.odometry.y > THROWING_ZONE_ENTRANCE_Y_23 - 0.10f)) {
							if (num_of_shagai_thrown == 0) {current_motion = PRE_PHASE_SHAGAI1_THROW;}
							else 						   {current_motion = PRE_PHASE_SHAGAI2_THROW;}
						} else {
							current_motion = MODE_MT;
						}
						shagaiautoflag = WAIT_NEXT;
					}
				} else if (PS_BtnChg(CONTROLLER_RIGHT)) {
					if (shagaiautoflag == NO_FLAG) {
						if (sw_field == FIELD_RED) {
							if (hypotf((((sw_field == FIELD_RED) ? (state.odometry.x) : (-state.odometry.x)) - SHAGAI_THROW_X), (state.odometry.y - SHAGAI_THROW_Y)) < 0.50f) {
								current_motion = PRE_PHASE_SHAGAI2_PICKUP;
							}
						} else {
							if (hypotf((((sw_field == FIELD_RED) ? (state.odometry.x) : (-state.odometry.x)) - SHAGAI_THROW_X), (state.odometry.y - SHAGAI_THROW_Y)) < 0.50f) {
								current_motion = PRE_PHASE_SHAGAI3_PICKUP;
							}
						}
						shagaiautoflag = WAIT_NEXT;
					}
				} else if (PS_BtnChg(CONTROLLER_LEFT)) {
					if (shagaiautoflag == NO_FLAG) {
						if (sw_field == FIELD_RED) {
							if (hypotf((((sw_field == FIELD_RED) ? (state.odometry.x) : (-state.odometry.x)) - SHAGAI_THROW_X), (state.odometry.y - SHAGAI_THROW_Y)) < 0.50f) {
								current_motion = PRE_PHASE_SHAGAI3_PICKUP;
							}
						} else {
							if (hypotf((((sw_field == FIELD_RED) ? (state.odometry.x) : (-state.odometry.x)) - SHAGAI_THROW_X), (state.odometry.y - SHAGAI_THROW_Y)) < 0.50f) {
								current_motion = PRE_PHASE_SHAGAI2_PICKUP;
							}
						}
						shagaiautoflag = WAIT_NEXT;
					}
				} else if (PS_BtnChg(CONTROLLER_SELECT) && (slavecom_cmd == State_ExtendPoker)) {
					if (shagaiautoflag == NO_FLAG) {
						current_motion = PRE_PHASE_SHAGAI_AUTOPICKUP;
						shagaiautoflag = WAIT_NEXT;
					}
				} else if (shagaiautoflag == WAIT_NEXT) {
					shagaiautoflag = NO_FLAG;
				}

				break;

			case MODE_LIDAR_SETUP:
				if(state.odometry.theta < DEG2RAD(359)){
					ref_pos.theta += 2*M_PI / 5 * CTRL_CYCLE;
					failsafe |= UnderMotor_Location_withPID(&state, &ref_pos);
//					failsafe |= UnderMotor_Velocity_withOmega(&state, &ref_pos);
				}
				else{
					UnderMotor_Stop();
					state.odometry.theta -= 2*M_PI;
					ref_pos.theta = 0;
					f1_flag = GET_FLAG;
				}
				break;

			//自動経路追従
			case PHASE_FOREST:
				nearest_path = CalcPurePursuit(&state, &ref_pos, &follow_states, sw_field, current_motion);
				failsafe |= UnderMotor_Velocity_withPID(&state, &ref_pos);

				if(nearest_path > PATH_AFTERBRIDGE && bridgeflag == NO_FLAG){
					slavecom_cmd = State_AfterBridge;
					slave_com_send(slavecom_cmd, 0);
					bridgeflag = GET_FLAG;
				}
				if(wally_flag == NO_FLAG){
					if ((HAL_GPIO_ReadPin(LIMIT1_GPIO_Port,LIMIT1_Pin) != GPIO_PIN_SET) && (HAL_GPIO_ReadPin(LIMIT2_GPIO_Port,LIMIT2_Pin) != GPIO_PIN_SET)) {
						//Y方向自己位置修正
						state.enc_odom.y = 8.37f;
						wally_flag = GET_FLAG;
						//スレーブにAfterLine指令
						slavecom_cmd = State_AfterLine;
						slave_com_send(slavecom_cmd, 0);
						//経路追従終了
						follow_states = PF_FINISH;
						Melody_Start(12);
					}
				}
				if(follow_states == PF_FINISH){
					current_motion = PRE_PHASE_GERGE;
				}
				break;
			case PHASE_GERGE:
				nearest_path = CalcPurePursuit(&state, &ref_pos, &follow_states, sw_field, current_motion);
				failsafe |= UnderMotor_Velocity_withPID(&state, &ref_pos);

				//ぶつかったら位置をリセットして経路追従終了
				if (wally_flag == GET_FLAG && wallx_flag == NO_FLAG && fabsf(state.odometry.x) > 5.5f && fabsf(state.vel.vx) < 0.10f) {
					if (sw_field==FIELD_RED) 	   {state.enc_odom.x = -6.13f;}
					else if (sw_field==FIELD_BLUE) {state.enc_odom.x = 6.13f;}
					state.enc_odom.y = 8.37f;
//					state.odometry.theta = 0;
					//経路追従終了
					follow_states = PF_FINISH;
					wallx_flag = GET_FLAG;
				}

				if (follow_states == PF_FINISH) {
					slavecom_cmd = State_AfterGerge;
					current_motion = PRE_PHASE_SHAGAI1_PICKUP;
				}
				break;
			case PHASE_SHAGAI1_PICKUP:
				nearest_path = CalcPurePursuit(&state, &ref_pos, &follow_states, sw_field, current_motion);
				failsafe |= UnderMotor_Velocity_withPID(&state, &ref_pos);

				if(follow_states == PF_FINISH){
					current_motion = MODE_MT;
					axis_dir = -1;
				}
				break;
			case PHASE_SHAGAI1_READY :
				if (ready_rotate_state == 0) {
					//経路追従
					nearest_path = CalcPurePursuit(&state, &ref_pos, &follow_states, sw_field, current_motion);
					//回転
					if (sw_field == FIELD_RED) {
						if (state.odometry.x < -3.7f) {		//垢フィールド危険域はL字棒が木枠に当たらないように111度までに回転を制限
							if (ref_pos.theta < DEG2RAD(111)) {ref_pos.theta += 1.5 * CTRL_CYCLE;}
						} else {							//危険域を過ぎたら最終角度まで回転してよい
							if (ref_pos.theta < shagai_throw_1[0].theta) {ref_pos.theta += 1.5 * CTRL_CYCLE;}
						}
					} else {
						if (ref_pos.theta < 2 * M_PI - shagai_throw_1[0].theta) {ref_pos.theta += 1.5 * CTRL_CYCLE;}	//青フィールドは制限なく回転
					}
					failsafe |= UnderMotor_Velocity_withPID(&state, &ref_pos);
					if (follow_states == PF_FINISH) {ready_rotate_state = 1;}
				} else if (ready_rotate_state == 1){
					if (sw_field == FIELD_RED) {
						if (ref_pos.theta < shagai_throw_1[0].theta) {ref_pos.theta += 1.5 * CTRL_CYCLE;}
						else 										 {ready_rotate_state = 2;}
					} else {
						if (ref_pos.theta < 2 * M_PI - shagai_throw_1[0].theta) {ref_pos.theta += 1.5 * CTRL_CYCLE;}
						else 													{ready_rotate_state = 2;}
					}
					ref_pos.x = ref_pos.y = 0;
					failsafe |= UnderMotor_Velocity_withPID(&state, &ref_pos);
				} else {
					current_motion = MODE_MT;
				}
				break;
			case PHASE_SHAGAI2_PICKUP :
			case PHASE_SHAGAI3_PICKUP :
				nearest_path = CalcPurePursuit(&state, &ref_pos, &follow_states, sw_field, current_motion);

				//角度だけ手動操作を受け付ける
				ref_pos.theta += 0.8f * CTRL_CYCLE * (-analog_right.x);
				if (PS_Btn(CONTROLLER_L1)) 	  {ref_pos.theta += 1.5f * CTRL_CYCLE;}
				else if (PS_Btn(CONTROLLER_R1)) {ref_pos.theta -= 1.5f * CTRL_CYCLE;}

				failsafe |= UnderMotor_Velocity_withPID(&state, &ref_pos);

				if(follow_states == PF_FINISH){
					current_motion = MODE_MT;
//					Melody_Start(5);
				}
				break;
			case PHASE_SHAGAI1_THROW :
				nearest_path = CalcPurePursuit(&state, &ref_pos, &follow_states, sw_field, current_motion);

				failsafe |= UnderMotor_Velocity_withPID(&state, &ref_pos);

				//準備
				if (state.odometry.y < POS_THROWING_READY && throwflag == NO_FLAG) {
					slavecom_cmd = State_ReadyToShoot;
					slave_com_send(slavecom_cmd, 0);
					throwflag = GET_FLAG;
				}

#ifdef YABUSAME
				//投げる
				if (state.odometry.y < POS_THROWING && slavecom_cmd == State_ReadyToShoot) {
					slavecom_cmd = State_ShootShagai;
					slave_com_send(slavecom_cmd, 0);
					throwflag = GET_FLAG;
					num_of_shagai_thrown = 1;
				}
				if (follow_states == PF_FINISH) {
					current_motion = PRE_PHASE_SHAGAI2_PICKUP;
//					current_motion = MODE_MT;
				}
#endif
#ifndef YABUSAME
				if (follow_states == PF_FINISH) {
					shagai_throw_state = 1;
				}

				if (shagai_throw_state == 1) {
					if (shagai_throw_time == 330) {
						slavecom_cmd = State_ShootShagai;
						slave_com_send(slavecom_cmd, 0);
						throwflag = GET_FLAG;
						num_of_shagai_thrown = 1;
					} else if (shagai_throw_time >= 800) {
						current_motion = PRE_PHASE_SHAGAI2_PICKUP;
					}
					shagai_throw_time += CTRL_CYCLE * 1000;
				}
#endif
				break;
			case PHASE_SHAGAI2_THROW :
			case PHASE_SHAGAI3_THROW :
				nearest_path = CalcPurePursuit(&state, &ref_pos, &follow_states, sw_field, current_motion);

				//角度だけ手動操作を受け付ける
				ref_pos.theta += 0.8f * CTRL_CYCLE * (-analog_right.x);
				if (PS_Btn(CONTROLLER_L1)) 	  {ref_pos.theta += 1.5f * CTRL_CYCLE;}
				else if (PS_Btn(CONTROLLER_R1)) {ref_pos.theta -= 1.5f * CTRL_CYCLE;}

				failsafe |= UnderMotor_Velocity_withPID(&state, &ref_pos);

				if (state.odometry.y < POS_THROWING_READY && throwflag == NO_FLAG) {
					slavecom_cmd = State_ReadyToShoot;
					slave_com_send(slavecom_cmd, 0);
					throwflag = GET_FLAG;
					Melody_Start(11);
				}

				if (follow_states == PF_FINISH) {
					current_motion = MODE_MT;
				}
				break;
			case PHASE_SHAGAI_AUTOPICKUP :
				//90度回りながらシャガイ掴む
				if ((ref_pos.theta < odom_theta_buf + DEG2RAD(45)) && (shagai_pickup_state <= 1)) {			//75度以下
					//回転速め
					ref_pos.theta += 0.88 * CTRL_CYCLE;
					//一定角度超えたところでL字棒ロック
					if ((ref_pos.theta > odom_theta_buf + DEG2RAD(40)) && (shagai_pickup_state == 0)) {
						slavecom_cmd = State_LockShagai;
						slave_com_send(slavecom_cmd, 0);
						shagai_pickup_state = 1;
					}
				} else if ((ref_pos.theta < odom_theta_buf + DEG2RAD(70)) && (shagai_pickup_state <= 1)) {	//90度以下
					//回転遅め
					ref_pos.theta += 0.66 * CTRL_CYCLE;
				} else {
					switch (shagai_pickup_state) {
					case 1 :
						if (shagai_pickup_time == 0) {
							slavecom_cmd = State_ReadyToGrasp;
							slave_com_send(slavecom_cmd, 0);
						} else if (shagai_pickup_time > 2800) {
							shagai_pickup_state = 2;
							shagai_pickup_time = -CTRL_CYCLE * 1000;
						}
						break;
					case 2 :
						if (shagai_pickup_time == 0) {
							slavecom_cmd = State_StorePoker;
							slave_com_send(slavecom_cmd, 0);
						} else if (shagai_pickup_time > 1400) {
							shagai_pickup_state = 3;
							shagai_pickup_time = -CTRL_CYCLE * 1000;
						}
						break;
					default :
						if (hypotf((((sw_field == FIELD_RED) ? (state.odometry.x) : (-state.odometry.x)) - SHAGAI1_PICKUP_X), (state.odometry.y - SHAGAI1_PICKUP_Y)) < 0.50f) {
							current_motion = PRE_PHASE_SHAGAI1_READY;
							Melody_Start(13);
						} else if ((hypotf((((sw_field == FIELD_RED) ? (state.odometry.x) : (-state.odometry.x)) - THROWING_ZONE_ENTRANCE_X_23), (state.odometry.y - THROWING_ZONE_ENTRANCE_Y_23)) < 3.34f)
								&& (state.odometry.y > THROWING_ZONE_ENTRANCE_Y_23 - 0.10f)) {
							current_motion = PRE_PHASE_SHAGAI2_THROW;
							Melody_Start(14);
						} else {
							current_motion = MODE_MT;
//							Melody_Start(5);
						}
						break;
					}
					shagai_pickup_time += CTRL_CYCLE * 1000;
				}
				ref_pos.x = ref_pos.y = 0;
				failsafe |= UnderMotor_Velocity_withPID(&state, &ref_pos);
				break;
			case PHASE_TEST_PATH :
				nearest_path = CalcPurePursuit(&state, &ref_pos, &follow_states, sw_field, current_motion);
				failsafe |= UnderMotor_Velocity_withPID(&state, &ref_pos);

				if(follow_states == PF_FINISH){
					current_motion = MODE_MT;
					axis_dir = -1;
				}
				break;

			//各PHASEの準備
			case PRE_PHASE_FOREST :
				ref_pos.theta = 0;
				break;
			case PRE_PHASE_GERGE :
			case PRE_PHASE_SHAGAI1_PICKUP :
				break;
			case PRE_PHASE_SHAGAI1_READY :
				ready_rotate_state = 0;
				break;
			case PRE_PHASE_SHAGAI2_PICKUP :
			case PRE_PHASE_SHAGAI3_PICKUP :
				break;
			case PRE_PHASE_SHAGAI1_THROW :
			case PRE_PHASE_SHAGAI2_THROW :
			case PRE_PHASE_SHAGAI3_THROW :
				throwflag = NO_FLAG;
				break;
			case PRE_PHASE_SHAGAI_AUTOPICKUP :
				shagai_pickup_state = 0;
				shagai_pickup_time = 0;
				//現在角度保存
				odom_theta_buf = ref_pos.theta;
				//PHASEへ
				current_motion = PHASE_SHAGAI_AUTOPICKUP;
				break;
			case PRE_PHASE_TEST_PATH :
				break;
			case PRE_MODE_MT :
				ref_pos.theta = 0;
				current_motion = MODE_MT;
				break;
			case PRE_MODE_LIDAR_SETUP :
				ref_pos.x = state.odometry.x;
				ref_pos.y =  state.odometry.y;
				ref_pos.theta = state.odometry.theta;
				ref_pos.omega=1.2;

				//MODEへ
				current_motion = MODE_LIDAR_SETUP;
				break;
			default:
				failsafe |= NO_MODE;
				break;
		}

		//フェールセーフで停止
		if(failsafe != NO_ERROR){
			UnderMotor_Stop();
			HAL_TIM_Base_Stop_IT(&TIM_CTRL_HANDLER);
		}

		if (time > 50) {
//			printf("%f %f\n\n", analog_right.r, RAD2DEG(analog_right.theta));
//			printf("%f %f\n", state.apply.omega, ref_pos.omega);
//			printf("%f %f %d\n", RAD2DEG(ref_pos.theta), RAD2DEG(state.odometry.theta), slavecom_cmd);
//			printf("%f %f %f\n", state.odometry.x, state.odometry.y, state.odometry.theta);
//			printf("%f %f\n", RAD2DEG(ref_pos.theta), RAD2DEG(state.odometry.theta));
//			printf("\n%d\n\n", current_motion);
//			printf("%d %d\n", (int)(hypotf(ref_pos.x, ref_pos.y) * 1000), (int)(state.vel.vabs * 1000));
//			printf("%f %f\n", ref_pos.x, ref_pos.y);
//			printf("%f %f\n", state.vel.vx, state.vel.vy);
			time = 0;
		} else {
			time += CTRL_CYCLE * 1000;
		}
	}
	//10ms Timer
	if (htim->Instance == TIM_10MS) {
		pre_button_data = button_data;
		if(sw_ctrl == CTRL_VSC3){
			//SPI communication with DUALSHOCK2
			PSControllerGetData(&button_data, &analog_left, &analog_right, vib_state, 0);
		}
		else{
			controller_com_timer();
			controller_com_get_data(&button_data , &analog_left, &analog_right);
		}

		//Process uart_com
		uart_com_timer();
//        if(sw_lidar == LIDAR_ON && current_motion != MODE_LIDAR_SETUP){
//            rosbridge_proxy_send();
//        }
		rosbridge_proxy_send();
		MX_LWIP_Process();
	}
}
/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  static ModeSWState select_mode = MODESW_0;		//select modeSW
  static ModeSWState pre_select_mode = -1;			//select modeSW

  //trajectory vary
  float side_g = 0;
  float max_accel = 0;

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_DMA_Init();
  MX_USART3_UART_Init();
  MX_TIM1_Init();
  MX_TIM3_Init();
  MX_TIM2_Init();
  MX_SPI3_Init();
  MX_I2C2_Init();
  MX_TIM6_Init();
  MX_TIM8_Init();
  MX_TIM4_Init();
  MX_TIM7_Init();
  MX_ADC3_Init();
  MX_TIM14_Init();
  MX_USART1_UART_Init();
  MX_LWIP_Init();
  MX_TIM5_Init();
  MX_TIM12_Init();
  MX_TIM13_Init();
  MX_TIM11_Init();
  MX_USART2_UART_Init();
  /* USER CODE BEGIN 2 */

  //---Start Robot Initialize---//

  //初期化中の警告音
  BuzzerOn(BUZZER_C8);

  //Initialize UART COM
  uart_com_init(&huart1);
  //Initialize DMA printf
  dma_printf_init(&huart3);
  //Initialize rosbridge proxy
  rosbridge_proxy_init();
  //Initiaize WiFi Controller
  controller_com_init(&huart2);
  //printf setting
  setbuf(stdout, NULL);
  printf("\nMR1 : Starting Up...\n");

  //state.odometry.x = ((sw_field == FIELD_RED) ? (1.0) : (-1.0));
  //state.odometry.y =  1.0;

  HAL_Delay(250);

  //Initialize OLED
  OLED_Init();

  //Initialize MPU9250
  MPU9250Init();
  MPU9250GetAngVelOffset();

  //Encoder Enable
  EncoderEnable();

  //Enable Odometry Timer
  HAL_TIM_Base_Start_IT(&TIM_ODOMETRY_HANDLER);

  //Initialize UnderMotor
  undermotor_init();

  //コントローラ
  failsafe = PSControllerInit(ANALOG_MODE,MODECHANGE_DIS);
  HAL_TIM_Base_Start_IT(&TIM_10MS_HANDLER);

  __HAL_TIM_CLEAR_IT(&TIM_MELODY_HANDLER, TIM_IT_UPDATE);

  printf("Initialize Finished\n");

  //初期化終了警告音停止
  BuzzerOff();

  //---finish Robot Initialize---//

  //Mode Set
  Mode_Set(&select_mode, MODESW_0);

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
	  //---Start Mode Select---//

	  //Start ADC for Volume  <- active while ModeSelect in order to print values in LCD.
	  HAL_ADC_Start_DMA(&hadc3, adc_data, 2);

	  f1_flag = NO_FLAG;
	  bridgeflag = NO_FLAG;
	  line1_flag = NO_FLAG;
	  wallx_flag = NO_FLAG;
	  wally_flag = NO_FLAG;
	  pram_flag = NO_FLAG;
	  melotest_flag = NO_FLAG;
	  //LED set
	  HAL_GPIO_WritePin(STARTLD_GPIO_Port,STARTLD_Pin, GPIO_PIN_RESET);
	  lchika_pin = SW_F1;
	  HAL_TIM_Base_Start_IT(&TIM_UI_HANDLER);

	  OLED_SetCursor(1,7);
	  OLED_Print("SideG");
	  OLED_SetCursor(2,7);
	  OLED_Print("Accel");
	  do {
		  //Select Start Mode
		  if (OPSwitchIsOn(SW_MODE1) == SWITCH_ON) {
			  if (select_mode == MODESW_1) Mode_Set(&select_mode, MODESW_0);
			  else						   Mode_Set(&select_mode, MODESW_1);
			  OPWaitSwitchOff(SW_MODE1);
		  }
		  if (OPSwitchIsOn(SW_MODE2) == SWITCH_ON) {
			  if (select_mode == MODESW_2) Mode_Set(&select_mode, MODESW_0);
			  else 						   Mode_Set(&select_mode, MODESW_2);
			  OPWaitSwitchOff(SW_MODE2);
		  }
		  if (OPSwitchIsOn(SW_MODE3) == SWITCH_ON) {
			  if (select_mode == MODESW_3) Mode_Set(&select_mode, MODESW_0);
			  else						   Mode_Set(&select_mode, MODESW_3);
			  OPWaitSwitchOff(SW_MODE3);
		  }
		  if (OPSwitchIsOn(SW_MODE4) == SWITCH_ON) {
			  if (select_mode == MODESW_4) Mode_Set(&select_mode, MODESW_0);
			  else						   Mode_Set(&select_mode, MODESW_4);
			  OPWaitSwitchOff(SW_MODE4);
		  }

		  //操作モード選択
		  if(OPSwitchIsOn(SW_TOGGLE1) == SWITCH_ON) sw_field	= FIELD_RED;
		  else										sw_field	= FIELD_BLUE;
		  if(OPSwitchIsOn(SW_TOGGLE2) == SWITCH_ON) sw_pilot	= PILOT_AT;
		  else										sw_pilot	= PILOT_MT;
		  if(OPSwitchIsOn(SW_TOGGLE3) == SWITCH_ON) sw_ctrl		= CTRL_WIFI;
		  else  									sw_ctrl		= CTRL_VSC3;
		  if(OPSwitchIsOn(SW_TOGGLE4) == SWITCH_ON) sw_lidar	= LIDAR_ON;
		  else  									sw_lidar	= LIDAR_OFF;

		  if(pre_select_mode!=select_mode || pre_sw_pilot!=sw_pilot){
			  pre_select_mode=select_mode;
			  pre_sw_pilot=sw_pilot;
			  OLED_SetCursor(1,1);
			  if (sw_pilot == PILOT_AT) {			//自動
				  switch(select_mode){
				  case MODESW_0 : OLED_Print("Forest");	break;
				  case MODESW_1 : OLED_Print("LINE1 ");	break;
				  case MODESW_2 : OLED_Print("LINE1T");	break;
				  case MODESW_3 : OLED_Print("LIDAR ");	break;
				  case MODESW_4 : OLED_Print("MANUAL");	break;
				  default: failsafe |= NO_MODE;
				  }
			  }
			  else{
				  switch(select_mode){
				  case MODESW_0 : OLED_Print("MANUAL");	break;
				  case MODESW_1 : OLED_Print("Pram  ");	break;
				  case MODESW_2 : OLED_Print("Theta ");	break;
				  case MODESW_3 : OLED_Print("Melody");	break;
				  case MODESW_4 : OLED_Print("MANUAL");	break;
				  default: failsafe |= NO_MODE;
				  }
			  }
		  }
		  if(pre_sw_field!=sw_field){
			  pre_sw_field=sw_field;
			  OLED_SetCursor(2,1);
			  if(sw_field == FIELD_RED)	OLED_Print("Red ");
			  else						OLED_Print("Blue");
		  }

		  //ボリュームで速度設定
		  side_g    = roundf(((float)adc_data[0] * 8 / 4096 + 1.0) * 10.0f) / 10.0f;
		  max_accel = roundf(((float)adc_data[1] * 8 / 4096 + 1.0) * 10.0f) / 10.0f;
		  OLED_SetCursor(1,12);
		  OLED_NumfPrint(side_g,1,2);
		  OLED_SetCursor(2,12);
		  OLED_NumfPrint(max_accel,1,2);
	  }while (OPSwitchIsOn(SW_F1) != SWITCH_ON);
	  OPWaitSwitchOff(SW_F1);
	  HAL_ADC_Stop_DMA(&hadc3);

	  //---finish Mode Select---//

	  //---Start Moving Settings---//
	  HAL_Delay(50);
	  //LED set
	  HAL_GPIO_WritePin(F1LD_GPIO_Port,F1LD_Pin, GPIO_PIN_SET);
	  lchika_pin = SW_START;

	  if (sw_pilot == PILOT_AT) {	//自動
		  switch(select_mode){
		  case MODESW_0 :
			  current_motion = PRE_PHASE_FOREST;
			  slavecom_cmd = State_Reset;
			  state.odometry.x = ((sw_field == FIELD_RED) ? (start_to_gerge[0].x) : (-start_to_gerge[0].x));
			  state.odometry.y =  start_to_gerge[0].y;
			  state.odometry.theta = start_to_gerge[0].theta;
			  break;
		  case MODESW_1 :
			  current_motion = MODE_MT;
			  state.odometry.x = ((sw_field == FIELD_RED) ? (-4.45) : (4.45));
			  state.odometry.y = 8.37f;
			  break;
		  case MODESW_2 :
			  current_motion = PRE_MODE_MT;
			  state.odometry.x = ((sw_field == FIELD_RED) ? (-4.45) : (4.45));
			  state.odometry.y = 8.37f;
			  state.odometry.theta = 0;
			  break;
		  case MODESW_3 :
			  current_motion = PRE_MODE_LIDAR_SETUP;
			  state.odometry.x = ((sw_field == FIELD_RED) ? (-1.0f) : (1.0f));
			  state.odometry.y =  1.0;
			  state.odometry.theta = 0;
			  break;
		  case MODESW_4 :
			  current_motion = MODE_MT;
		 	  break;
		  default: failsafe |= NO_MODE;
		  }
		  state.enc_odom.x = state.odometry.x;
		  state.enc_odom.y = state.odometry.y;
		  Start_Buzzer(select_mode);	//mode決定音
	  } else if (sw_pilot == PILOT_MT) {	//手動
		  switch(select_mode){
		  case MODESW_0 :
			  current_motion = MODE_MT;
			  Start_Buzzer(5);	//mode決定音
			  break;
		  case MODESW_1 :							//shagai angle test
			  current_motion = MODE_MT;
			  slavecom_cmd = State_AfterGerge;
			  pram_flag = GET_FLAG;
			  Start_Buzzer(6);	//mode決定音
			  break;
		  case MODESW_2 :							//MT Set Theta=0
			  current_motion = PRE_MODE_MT;
			  state.odometry.theta = 0;
			  Start_Buzzer(5);	//mode決定音
			  break;
		  case MODESW_3 :							//Melody_test
			  current_motion = MODE_MT;
			  melotest_flag = GET_FLAG;
			  Start_Buzzer(7);	//mode決定音
			  break;
		  default: failsafe |= NO_MODE;
		  }

	  }
	  //Send Init to Slave & Field Info
	  slave_com_send(State_Reset, sw_field);

	  //---Finish Moving Settings---//

	  while (OPSwitchIsOn(SW_START) != SWITCH_ON){
		  if(OPSwitchIsOn(SW_F1) == SWITCH_ON){
			  f1_flag = GET_FLAG;
			  break;
		  }
	  }
	  //stop StartLD Blink
	  HAL_TIM_Base_Stop_IT(&TIM_UI_HANDLER);
	  HAL_GPIO_WritePin(STARTLD_GPIO_Port,STARTLD_Pin, GPIO_PIN_SET);


	  //---Start Moving---//

	  //動作開始
	  if (sw_pilot == PILOT_AT) {			//自動
		  Melody_Start(select_mode);
	  }
	  else{
		  if(pram_flag == GET_FLAG){//shagai Angle test
			  HAL_ADC_Start_DMA(&hadc3, adc_data, 2);
			  HAL_TIM_Base_Start_IT(&TIM_50MS_HANDLER);
			  OLED_SetCursor(1,7);
			  OLED_Print(" Dyna");
			  OLED_SetCursor(2,7);
			  OLED_Print(" Angl");
		  }
			  Melody_Start(5+select_mode);
	  }
	  failsafe = NO_ERROR;
	  HAL_TIM_Base_Start_IT(&TIM_CTRL_HANDLER);		//制御割り込み開始

	  //main moving loop
	  while(f1_flag == NO_FLAG){
		  if(melotest_flag == GET_FLAG){
			  OPWaitSwitchOff(SW_START);
			  OPWaitSwitchOff(SW_START);
			  for(int i=0; i<16 && f1_flag == NO_FLAG; i++){
				  while(OPSwitchIsOn(SW_START) != SWITCH_ON){
					  if(OPSwitchIsOn(SW_F1) == SWITCH_ON){
						  f1_flag = GET_FLAG;
						  break;
					  }
				  }
				  Melody_Start(i);
				  OPWaitSwitchOff(SW_START);
				  OPWaitSwitchOff(SW_START);
				  OPWaitSwitchOff(SW_START);
			  }
		  }
		  switch(current_motion){
		  case PRE_PHASE_SHAGAI1_READY:
			  slavecom_cmd = State_StorePoker;
			  slave_com_send(slavecom_cmd, 0);
			  break;
		  case PRE_PHASE_SHAGAI1_THROW:
			  shagai_throw_state = 0;
			  shagai_throw_time = 0;
			  throwflag = NO_FLAG;
			  break;
		  case PRE_PHASE_SHAGAI2_THROW:
			  throwflag = NO_FLAG;
			  break;
		  case PRE_PHASE_SHAGAI3_THROW:
			  throwflag = NO_FLAG;
			  break;
		  default:
			  break;
		  }
		  //経路切替・速度マップ生成
		  if (current_motion == PRE_PHASE_SHAGAI1_THROW) {
#ifdef YABUSAME
			  PrepareNextPath(&current_motion, 5.0, 2.2f, 2.2f, state, sw_field);
#endif
#ifndef YABUSAME
			  PrepareNextPath(&current_motion, 5.0, max_accel, side_g, state, sw_field);
#endif
		  } else {
			  PrepareNextPath(&current_motion, 5.0, max_accel, side_g, state, sw_field);
		  }

		  if(pram_flag == GET_FLAG && LCD_flag == GET_FLAG){
			  OLED_SetCursor(1,13);
			  OLED_NumPrint(dyna_angle,3);
			  OLED_SetCursor(2,13);
			  OLED_NumPrint(rail_angle,3);
			  LCD_flag = NO_FLAG;
		  }
		  //フェールセーフで停止
		  if(failsafe != NO_ERROR){
			  UnderMotor_Stop();
			  HAL_TIM_Base_Stop_IT(&TIM_CTRL_HANDLER);
			  HAL_TIM_Base_Stop_IT(&TIM_50MS_HANDLER);
			  slave_com_send(State_NULL,0);
			  vib_state = VIB_SMALL_MOTOR_ON;
			  switch(failsafe){
			  case FINISH_PROCESS:
				  printf("Finish Process");
				  break;
			  case CANCEL_PROCESS:
				  printf("Cancel Process");
				  break;
			  case TOO_DIFF_FROM_REF:
				  printf("too far from Ref");
				  break;
			  case OVER_SPEED:
				  printf("Speed Over");
				  break;
			  case NO_MODE:
				  printf("NO Mode");
				  break;
			  case FAIL_CONNECTION:
				  printf("Fail Connection");
				  break;
			  case UNKOWN_ERROR:
				  printf("UNKOWN");
				  break;
			  default:
				  printf("Multiple Error");
				  break;
			  }
			  HAL_TIM_Base_Stop_IT(&TIM_MELODY_HANDLER);
			  Melody(failsafe);
			  while(OPSwitchIsOn(SW_F1) != SWITCH_ON);
		  }
		  if(OPSwitchIsOn(SW_F1) == SWITCH_ON){
			  break;
		  }
	  }
	  HAL_TIM_Base_Stop_IT(&TIM_CTRL_HANDLER);
	  HAL_TIM_Base_Stop_IT(&TIM_50MS_HANDLER);
	  HAL_TIM_Base_Stop_IT(&TIM_MELODY_HANDLER);
	  vib_state = VIB_SMALL_MOTOR_OFF;
	  BuzzerBeep(BUZZER_Bm6,100);
	  BuzzerBeep(BUZZER_C6,100);
	  OPWaitSwitchOff(SW_F1);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = {0};

  /** Configure LSE Drive Capability 
  */
  HAL_PWR_EnableBkUpAccess();
  /** Configure the main internal regulator output voltage 
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 4;
  RCC_OscInitStruct.PLL.PLLN = 216;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 6;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Activate the Over-Drive mode 
  */
  if (HAL_PWREx_EnableOverDrive() != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV4;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_7) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_USART2
                              |RCC_PERIPHCLK_USART3|RCC_PERIPHCLK_I2C2;
  PeriphClkInitStruct.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInitStruct.Usart2ClockSelection = RCC_USART2CLKSOURCE_PCLK1;
  PeriphClkInitStruct.Usart3ClockSelection = RCC_USART3CLKSOURCE_PCLK1;
  PeriphClkInitStruct.I2c2ClockSelection = RCC_I2C2CLKSOURCE_PCLK1;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC3_Init(void)
{

  /* USER CODE BEGIN ADC3_Init 0 */

  /* USER CODE END ADC3_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC3_Init 1 */

  /* USER CODE END ADC3_Init 1 */
  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion) 
  */
  hadc3.Instance = ADC3;
  hadc3.Init.ClockPrescaler = ADC_CLOCK_SYNC_PCLK_DIV4;
  hadc3.Init.Resolution = ADC_RESOLUTION_12B;
  hadc3.Init.ScanConvMode = ENABLE;
  hadc3.Init.ContinuousConvMode = ENABLE;
  hadc3.Init.DiscontinuousConvMode = DISABLE;
  hadc3.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc3.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc3.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc3.Init.NbrOfConversion = 2;
  hadc3.Init.DMAContinuousRequests = ENABLE;
  hadc3.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  if (HAL_ADC_Init(&hadc3) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_8;
  sConfig.Rank = ADC_REGULAR_RANK_1;
  sConfig.SamplingTime = ADC_SAMPLETIME_84CYCLES;
  if (HAL_ADC_ConfigChannel(&hadc3, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure for the selected ADC regular channel its corresponding rank in the sequencer and its sample time. 
  */
  sConfig.Channel = ADC_CHANNEL_14;
  sConfig.Rank = ADC_REGULAR_RANK_2;
  if (HAL_ADC_ConfigChannel(&hadc3, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC3_Init 2 */

  /* USER CODE END ADC3_Init 2 */

}

/**
  * @brief I2C2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C2_Init(void)
{

  /* USER CODE BEGIN I2C2_Init 0 */

  /* USER CODE END I2C2_Init 0 */

  /* USER CODE BEGIN I2C2_Init 1 */

  /* USER CODE END I2C2_Init 1 */
  hi2c2.Instance = I2C2;
  hi2c2.Init.Timing = 0x20404768;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c2) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Analogue filter 
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c2, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }
  /** Configure Digital filter 
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c2, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C2_Init 2 */

  /* USER CODE END I2C2_Init 2 */

}

/**
  * @brief SPI3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_SPI3_Init(void)
{

  /* USER CODE BEGIN SPI3_Init 0 */

  /* USER CODE END SPI3_Init 0 */

  /* USER CODE BEGIN SPI3_Init 1 */

  /* USER CODE END SPI3_Init 1 */
  /* SPI3 parameter configuration*/
  hspi3.Instance = SPI3;
  hspi3.Init.Mode = SPI_MODE_MASTER;
  hspi3.Init.Direction = SPI_DIRECTION_2LINES;
  hspi3.Init.DataSize = SPI_DATASIZE_8BIT;
  hspi3.Init.CLKPolarity = SPI_POLARITY_HIGH;
  hspi3.Init.CLKPhase = SPI_PHASE_2EDGE;
  hspi3.Init.NSS = SPI_NSS_SOFT;
  hspi3.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_256;
  hspi3.Init.FirstBit = SPI_FIRSTBIT_MSB;
  hspi3.Init.TIMode = SPI_TIMODE_DISABLE;
  hspi3.Init.CRCCalculation = SPI_CRCCALCULATION_DISABLE;
  hspi3.Init.CRCPolynomial = 7;
  hspi3.Init.CRCLength = SPI_CRC_LENGTH_DATASIZE;
  hspi3.Init.NSSPMode = SPI_NSS_PULSE_DISABLE;
  if (HAL_SPI_Init(&hspi3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN SPI3_Init 2 */

  /* USER CODE END SPI3_Init 2 */

}

/**
  * @brief TIM1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM1_Init(void)
{

  /* USER CODE BEGIN TIM1_Init 0 */

  /* USER CODE END TIM1_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM1_Init 1 */

  /* USER CODE END TIM1_Init 1 */
  htim1.Instance = TIM1;
  htim1.Init.Prescaler = 1;
  htim1.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim1.Init.Period = 8000;
  htim1.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim1.Init.RepetitionCounter = 0;
  htim1.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim1) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim1, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim1, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.BreakFilter = 0;
  sBreakDeadTimeConfig.Break2State = TIM_BREAK2_DISABLE;
  sBreakDeadTimeConfig.Break2Polarity = TIM_BREAK2POLARITY_HIGH;
  sBreakDeadTimeConfig.Break2Filter = 0;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim1, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM1_Init 2 */

  /* USER CODE END TIM1_Init 2 */
  HAL_TIM_MspPostInit(&htim1);

}

/**
  * @brief TIM2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM2_Init(void)
{

  /* USER CODE BEGIN TIM2_Init 0 */

  /* USER CODE END TIM2_Init 0 */

  TIM_Encoder_InitTypeDef sConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM2_Init 1 */

  /* USER CODE END TIM2_Init 1 */
  htim2.Instance = TIM2;
  htim2.Init.Prescaler = 0;
  htim2.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim2.Init.Period = 65535;
  htim2.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim2.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  sConfig.EncoderMode = TIM_ENCODERMODE_TI12;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = 0;
  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC2Filter = 0;
  if (HAL_TIM_Encoder_Init(&htim2, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim2, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM2_Init 2 */

  /* USER CODE END TIM2_Init 2 */

}

/**
  * @brief TIM3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM3_Init(void)
{

  /* USER CODE BEGIN TIM3_Init 0 */

  /* USER CODE END TIM3_Init 0 */

  TIM_Encoder_InitTypeDef sConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM3_Init 1 */

  /* USER CODE END TIM3_Init 1 */
  htim3.Instance = TIM3;
  htim3.Init.Prescaler = 0;
  htim3.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim3.Init.Period = 65535;
  htim3.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim3.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  sConfig.EncoderMode = TIM_ENCODERMODE_TI12;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = 0;
  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC2Filter = 0;
  if (HAL_TIM_Encoder_Init(&htim3, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim3, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM3_Init 2 */

  /* USER CODE END TIM3_Init 2 */

}

/**
  * @brief TIM4 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM4_Init(void)
{

  /* USER CODE BEGIN TIM4_Init 0 */

  /* USER CODE END TIM4_Init 0 */

  TIM_Encoder_InitTypeDef sConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM4_Init 1 */

  /* USER CODE END TIM4_Init 1 */
  htim4.Instance = TIM4;
  htim4.Init.Prescaler = 20;
  htim4.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim4.Init.Period = 65535;
  htim4.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim4.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  sConfig.EncoderMode = TIM_ENCODERMODE_TI12;
  sConfig.IC1Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC1Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC1Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC1Filter = 0;
  sConfig.IC2Polarity = TIM_ICPOLARITY_RISING;
  sConfig.IC2Selection = TIM_ICSELECTION_DIRECTTI;
  sConfig.IC2Prescaler = TIM_ICPSC_DIV1;
  sConfig.IC2Filter = 0;
  if (HAL_TIM_Encoder_Init(&htim4, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim4, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM4_Init 2 */

  /* USER CODE END TIM4_Init 2 */

}

/**
  * @brief TIM5 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM5_Init(void)
{

  /* USER CODE BEGIN TIM5_Init 0 */

  /* USER CODE END TIM5_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};
  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM5_Init 1 */

  /* USER CODE END TIM5_Init 1 */
  htim5.Instance = TIM5;
  htim5.Init.Prescaler = 108-1;
  htim5.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim5.Init.Period = 1000-1;
  htim5.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim5.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim5) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim5, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim5, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM5_Init 2 */

  /* USER CODE END TIM5_Init 2 */

}

/**
  * @brief TIM6 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM6_Init(void)
{

  /* USER CODE BEGIN TIM6_Init 0 */

  /* USER CODE END TIM6_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM6_Init 1 */

  /* USER CODE END TIM6_Init 1 */
  htim6.Instance = TIM6;
  htim6.Init.Prescaler = 108-1;
  htim6.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim6.Init.Period = 10000-1;
  htim6.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim6) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim6, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM6_Init 2 */

  /* USER CODE END TIM6_Init 2 */

}

/**
  * @brief TIM7 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM7_Init(void)
{

  /* USER CODE BEGIN TIM7_Init 0 */

  /* USER CODE END TIM7_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};

  /* USER CODE BEGIN TIM7_Init 1 */

  /* USER CODE END TIM7_Init 1 */
  htim7.Instance = TIM7;
  htim7.Init.Prescaler = 108-1;
  htim7.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim7.Init.Period = 10000-1;
  htim7.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim7) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim7, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM7_Init 2 */

  /* USER CODE END TIM7_Init 2 */

}

/**
  * @brief TIM8 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM8_Init(void)
{

  /* USER CODE BEGIN TIM8_Init 0 */

  /* USER CODE END TIM8_Init 0 */

  TIM_MasterConfigTypeDef sMasterConfig = {0};
  TIM_OC_InitTypeDef sConfigOC = {0};
  TIM_BreakDeadTimeConfigTypeDef sBreakDeadTimeConfig = {0};

  /* USER CODE BEGIN TIM8_Init 1 */

  /* USER CODE END TIM8_Init 1 */
  htim8.Instance = TIM8;
  htim8.Init.Prescaler = 0;
  htim8.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim8.Init.Period = 8000;
  htim8.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim8.Init.RepetitionCounter = 0;
  htim8.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_PWM_Init(&htim8) != HAL_OK)
  {
    Error_Handler();
  }
  sMasterConfig.MasterOutputTrigger = TIM_TRGO_RESET;
  sMasterConfig.MasterOutputTrigger2 = TIM_TRGO2_RESET;
  sMasterConfig.MasterSlaveMode = TIM_MASTERSLAVEMODE_DISABLE;
  if (HAL_TIMEx_MasterConfigSynchronization(&htim8, &sMasterConfig) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCNPolarity = TIM_OCNPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  sConfigOC.OCIdleState = TIM_OCIDLESTATE_RESET;
  sConfigOC.OCNIdleState = TIM_OCNIDLESTATE_RESET;
  if (HAL_TIM_PWM_ConfigChannel(&htim8, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim8, &sConfigOC, TIM_CHANNEL_2) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim8, &sConfigOC, TIM_CHANNEL_3) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_ConfigChannel(&htim8, &sConfigOC, TIM_CHANNEL_4) != HAL_OK)
  {
    Error_Handler();
  }
  sBreakDeadTimeConfig.OffStateRunMode = TIM_OSSR_DISABLE;
  sBreakDeadTimeConfig.OffStateIDLEMode = TIM_OSSI_DISABLE;
  sBreakDeadTimeConfig.LockLevel = TIM_LOCKLEVEL_OFF;
  sBreakDeadTimeConfig.DeadTime = 0;
  sBreakDeadTimeConfig.BreakState = TIM_BREAK_DISABLE;
  sBreakDeadTimeConfig.BreakPolarity = TIM_BREAKPOLARITY_HIGH;
  sBreakDeadTimeConfig.BreakFilter = 0;
  sBreakDeadTimeConfig.Break2State = TIM_BREAK2_DISABLE;
  sBreakDeadTimeConfig.Break2Polarity = TIM_BREAK2POLARITY_HIGH;
  sBreakDeadTimeConfig.Break2Filter = 0;
  sBreakDeadTimeConfig.AutomaticOutput = TIM_AUTOMATICOUTPUT_DISABLE;
  if (HAL_TIMEx_ConfigBreakDeadTime(&htim8, &sBreakDeadTimeConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM8_Init 2 */

  /* USER CODE END TIM8_Init 2 */
  HAL_TIM_MspPostInit(&htim8);

}

/**
  * @brief TIM11 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM11_Init(void)
{

  /* USER CODE BEGIN TIM11_Init 0 */

  /* USER CODE END TIM11_Init 0 */

  /* USER CODE BEGIN TIM11_Init 1 */

  /* USER CODE END TIM11_Init 1 */
  htim11.Instance = TIM11;
  htim11.Init.Prescaler = 216-1;
  htim11.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim11.Init.Period = 50000-1;
  htim11.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim11.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim11) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM11_Init 2 */

  /* USER CODE END TIM11_Init 2 */

}

/**
  * @brief TIM12 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM12_Init(void)
{

  /* USER CODE BEGIN TIM12_Init 0 */

  /* USER CODE END TIM12_Init 0 */

  TIM_ClockConfigTypeDef sClockSourceConfig = {0};

  /* USER CODE BEGIN TIM12_Init 1 */

  /* USER CODE END TIM12_Init 1 */
  htim12.Instance = TIM12;
  htim12.Init.Prescaler = 10800-1;
  htim12.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim12.Init.Period = 5000-1;
  htim12.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim12.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim12) != HAL_OK)
  {
    Error_Handler();
  }
  sClockSourceConfig.ClockSource = TIM_CLOCKSOURCE_INTERNAL;
  if (HAL_TIM_ConfigClockSource(&htim12, &sClockSourceConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM12_Init 2 */

  /* USER CODE END TIM12_Init 2 */

}

/**
  * @brief TIM13 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM13_Init(void)
{

  /* USER CODE BEGIN TIM13_Init 0 */

  /* USER CODE END TIM13_Init 0 */

  /* USER CODE BEGIN TIM13_Init 1 */

  /* USER CODE END TIM13_Init 1 */
  htim13.Instance = TIM13;
  htim13.Init.Prescaler = 108-1;
  htim13.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim13.Init.Period = 10000-1;
  htim13.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim13.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim13) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM13_Init 2 */

  /* USER CODE END TIM13_Init 2 */

}

/**
  * @brief TIM14 Initialization Function
  * @param None
  * @retval None
  */
static void MX_TIM14_Init(void)
{

  /* USER CODE BEGIN TIM14_Init 0 */

  /* USER CODE END TIM14_Init 0 */

  TIM_OC_InitTypeDef sConfigOC = {0};

  /* USER CODE BEGIN TIM14_Init 1 */

  /* USER CODE END TIM14_Init 1 */
  htim14.Instance = TIM14;
  htim14.Init.Prescaler = 108;
  htim14.Init.CounterMode = TIM_COUNTERMODE_UP;
  htim14.Init.Period = 0;
  htim14.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1;
  htim14.Init.AutoReloadPreload = TIM_AUTORELOAD_PRELOAD_DISABLE;
  if (HAL_TIM_Base_Init(&htim14) != HAL_OK)
  {
    Error_Handler();
  }
  if (HAL_TIM_PWM_Init(&htim14) != HAL_OK)
  {
    Error_Handler();
  }
  sConfigOC.OCMode = TIM_OCMODE_PWM1;
  sConfigOC.Pulse = 0;
  sConfigOC.OCPolarity = TIM_OCPOLARITY_HIGH;
  sConfigOC.OCFastMode = TIM_OCFAST_DISABLE;
  if (HAL_TIM_PWM_ConfigChannel(&htim14, &sConfigOC, TIM_CHANNEL_1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN TIM14_Init 2 */

  /* USER CODE END TIM14_Init 2 */
  HAL_TIM_MspPostInit(&htim14);

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 115200;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART2_UART_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2.Instance = USART2;
  huart2.Init.BaudRate = 460800;
  huart2.Init.WordLength = UART_WORDLENGTH_8B;
  huart2.Init.StopBits = UART_STOPBITS_1;
  huart2.Init.Parity = UART_PARITY_NONE;
  huart2.Init.Mode = UART_MODE_TX_RX;
  huart2.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2.Init.OverSampling = UART_OVERSAMPLING_16;
  huart2.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart2.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart2) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART3_UART_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3.Instance = USART3;
  huart3.Init.BaudRate = 115200;
  huart3.Init.WordLength = UART_WORDLENGTH_8B;
  huart3.Init.StopBits = UART_STOPBITS_1;
  huart3.Init.Parity = UART_PARITY_NONE;
  huart3.Init.Mode = UART_MODE_TX_RX;
  huart3.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3.Init.OverSampling = UART_OVERSAMPLING_16;
  huart3.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart3.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart3) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

/** 
  * Enable DMA controller clock
  */
static void MX_DMA_Init(void) 
{
  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();
  __HAL_RCC_DMA1_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA1_Stream1_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream1_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream1_IRQn);
  /* DMA1_Stream3_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream3_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream3_IRQn);
  /* DMA1_Stream5_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream5_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream5_IRQn);
  /* DMA1_Stream6_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA1_Stream6_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA1_Stream6_IRQn);
  /* DMA2_Stream0_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream0_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream0_IRQn);
  /* DMA2_Stream2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream2_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream2_IRQn);
  /* DMA2_Stream7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream7_IRQn, 0, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream7_IRQn);

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOC_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOH_CLK_ENABLE();
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(CS3_GPIO_Port, CS3_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOF, PHASE2_Pin|STARTLD_Pin|PHASE3_Pin|RST2_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(F1LD_GPIO_Port, F1LD_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(MODE3LD_GPIO_Port, MODE3LD_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOG, MODE2LD_Pin|MODE1LD_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOE, PHASE4_Pin|RST4_Pin|RST3_Pin|LED4_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, INB7_Pin|INA7_Pin|INA6_Pin|LD2_Pin 
                          |INA8_Pin|INB8_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(MODE4LD_GPIO_Port, MODE4LD_Pin, GPIO_PIN_SET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, PHASE1_Pin|RST1_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOG, LED2_Pin|LED1_Pin|LED3_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOA, INB6_Pin|INB5_Pin|INA5_Pin, GPIO_PIN_RESET);

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOD, CS1_Pin|CS2_Pin, GPIO_PIN_SET);

  /*Configure GPIO pins : CS3_Pin PHASE4_Pin RST4_Pin RST3_Pin 
                           LED4_Pin */
  GPIO_InitStruct.Pin = CS3_Pin|PHASE4_Pin|RST4_Pin|RST3_Pin 
                          |LED4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pins : TOGGLE2_Pin STARTSW_Pin TOGGLE1_Pin */
  GPIO_InitStruct.Pin = TOGGLE2_Pin|STARTSW_Pin|TOGGLE1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /*Configure GPIO pin : USER_Btn_Pin */
  GPIO_InitStruct.Pin = USER_Btn_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(USER_Btn_GPIO_Port, &GPIO_InitStruct);

  /*Configure GPIO pins : PHASE2_Pin F1LD_Pin STARTLD_Pin PHASE3_Pin 
                           RST2_Pin */
  GPIO_InitStruct.Pin = PHASE2_Pin|F1LD_Pin|STARTLD_Pin|PHASE3_Pin 
                          |RST2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /*Configure GPIO pins : LIMIT2_Pin LIMIT1_Pin */
  GPIO_InitStruct.Pin = LIMIT2_Pin|LIMIT1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

  /*Configure GPIO pins : MODE2SW_Pin BUTTON1_Pin */
  GPIO_InitStruct.Pin = MODE2SW_Pin|BUTTON1_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : MODE3LD_Pin INB6_Pin INB5_Pin INA5_Pin */
  GPIO_InitStruct.Pin = MODE3LD_Pin|INB6_Pin|INB5_Pin|INA5_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

  /*Configure GPIO pins : MODE4SW_Pin MODE3SW_Pin F1SW_Pin */
  GPIO_InitStruct.Pin = MODE4SW_Pin|MODE3SW_Pin|F1SW_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : MODE2LD_Pin LED2_Pin LED1_Pin LED3_Pin 
                           MODE1LD_Pin */
  GPIO_InitStruct.Pin = MODE2LD_Pin|LED2_Pin|LED1_Pin|LED3_Pin 
                          |MODE1LD_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /*Configure GPIO pins : INB7_Pin INA7_Pin MODE4LD_Pin INA6_Pin 
                           LD2_Pin INA8_Pin INB8_Pin */
  GPIO_InitStruct.Pin = INB7_Pin|INA7_Pin|MODE4LD_Pin|INA6_Pin 
                          |LD2_Pin|INA8_Pin|INB8_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pins : PHASE1_Pin RST1_Pin CS1_Pin CS2_Pin */
  GPIO_InitStruct.Pin = PHASE1_Pin|RST1_Pin|CS1_Pin|CS2_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);

  /*Configure GPIO pins : BUTTON3_Pin BUTTON2_Pin TOGGLE3_Pin MODE1SW_Pin 
                           TOGGLE4_Pin */
  GPIO_InitStruct.Pin = BUTTON3_Pin|BUTTON2_Pin|TOGGLE3_Pin|MODE1SW_Pin 
                          |TOGGLE4_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

}

/* USER CODE BEGIN 4 */
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *UartHandle)
{
	uart_flag=1;
	HAL_UART_Receive_IT(&huart3, Uart_Buf, 4);
}
void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart){
    uart_com_send_it(huart);
    dma_printf_send_it(huart);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

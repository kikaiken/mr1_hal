/*
 * robotstate.c
 *
 *  Created on: 2018/06/21
 *      Author: Ryohei
 */

#include "robotstate.h"

void ClearRobotState(struct_state *state) {
//	state->prev_vel = 0;
//	state->vel = 0;
	state->angvel = 0;
//	state->angvel_err_integ = 0;
	state->odometry.x = 0;
	state->odometry.y = 0;
	state->odometry.theta = 0;
//	state->battery_v = 0;
}

/*
 * rosbridge_proxy.c
 *
 *  Created on: 2019/04/29
 *      Author: naoki
 */

#include "main.h"
#include "rosbridge_proxy.h"
#include <stdio.h>
#include "lwip.h"
#include "lwip/udp.h"
#include <string.h>
#include "robotstate.h"
#include <math.h>
#include <stdlib.h>
#include "user_interface.h"
#include "mode.h"


struct udp_pcb *proxy_pcb;
ip4_addr_t proxy_ip;
extern struct_state state;
void rosbridge_proxy_init(){
	proxy_pcb = udp_new();
	IP4_ADDR(&proxy_ip, PROXY_IP_ADDR1, PROXY_IP_ADDR2, PROXY_IP_ADDR3, PROXY_IP_ADDR4);
	udp_bind(proxy_pcb, IP_ADDR_ANY, F7_PORT);
	udp_recv(proxy_pcb, (void *)rosbridge_proxy_recv, NULL);
}

void rosbridge_proxy_send(){
	struct pkt_odom odom;
	memset(&odom,0,sizeof(struct pkt_odom));
	odom.pose.point.x = state.odometry.x;
	odom.pose.point.y = state.odometry.y;
	odom.pose.orientation.x = state.odometry.theta;
	rosbridge_proxy_send_odom(&odom);
	//printf("Send");
}

void rosbridge_proxy_recv(void *arg, struct udp_pcb *pcb, struct pbuf *p, ip_addr_t *addr, u16_t port){
	struct pkt_pose2d *pose2d = (struct pkt_pose2d *)p->payload;
	if(pose2d->opcode != PROXY_DATA_POSE2D){
		return;
	}
	state.lidar_odom.x = pose2d->x;
	state.lidar_odom.y = pose2d->y;

	pbuf_free(p);
}

void rosbridge_proxy_send_odom(struct pkt_odom *odom){
	struct pbuf *p;
	odom->opcode = PROXY_DATA_ODOM;
	p = pbuf_alloc(PBUF_TRANSPORT, sizeof(struct pkt_odom), PBUF_RAM);
	memcpy(p->payload, odom, sizeof(struct pkt_odom));
	udp_sendto(proxy_pcb, p, &proxy_ip, PROXY_PORT);
	pbuf_free(p);
}

void rosbridge_proxy_send_tf(struct pkt_tf *tf){
	struct pbuf *p;
	tf->opcode = PROXY_DATA_TF;
	p = pbuf_alloc(PBUF_TRANSPORT, sizeof(struct pkt_tf), PBUF_RAM);
	memcpy(p->payload, tf, sizeof(struct pkt_tf));
	udp_sendto(proxy_pcb, p, &proxy_ip, PROXY_PORT);
	pbuf_free(p);
}

/*
 * mode.c
 *
 *  Created on: 2018/12/30
 *      Author: nabeya11
 */

#include "mode.h"
#include "user_interface.h"

/*
 * モードに対するブザー・LED
 * @param *mode	モード変数のポインタ
 * @return
 */
void Mode_UI(ModeSWState mode){
	switch(mode){
		case MODESW_0:
			BuzzerBeep(BUZZER_C6,100);
			OPLEDSet(0b1111);
			break;
		case MODESW_1:
			BuzzerBeep(BUZZER_D6,100);
			OPLEDSet(0b0001);
			break;
		case MODESW_2:
			BuzzerBeep(BUZZER_E6,100);
			OPLEDSet(0b0010);
			break;
		case MODESW_3:
			BuzzerBeep(BUZZER_F6,100);
			OPLEDSet(0b0100);
			break;
		case MODESW_4:
			BuzzerBeep(BUZZER_G6,100);
			OPLEDSet(0b1000);
			break;
		default:
			UIWarning();
			break;
	}
}

/*
 * モードスタートに対するブザー
 * @param	mode : モード変数のポインタ
 * @return
 */
void Start_Buzzer(ModeSWState mode){
	Arpeggio((uint8_t)mode);
}

/*
 * モード変更
 * @param *mode	モード変数のポインタ
 * @param  dir	モード加減算
 * @return
 */
void Mode_Change(ModeSWState *mode, ModeDirTypeDef dir){
	if(dir == MODE_PLUS){
		*mode == MODESW_MAX ? *mode = MODESW_MIN : (*mode)++;
	}
	else if(dir == MODE_MINUS){
		*mode == MODESW_MIN ? *mode = MODESW_MAX : (*mode)--;
	}
	else{
		*mode = MODESW_MIN;
	}
	Mode_UI(*mode);
}

/*
 * モード変更
 * @param *mode	モード変数のポインタ
 * @param  setmode	設定するモード
 * @return
 */
void Mode_Set(ModeSWState *mode, ModeSWState setmode){
	*mode = setmode;
	Mode_UI(*mode);
}

void Phase_Shift(PhaseAndMode *phase, PhaseAndMode setphase){
	*phase = setphase;
	Mode_UI(*phase);
}

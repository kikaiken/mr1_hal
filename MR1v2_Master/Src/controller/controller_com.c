/*
 * dynamixel_com.c
 *
 *  Created on: 2019/03/26
 *      Author: naoki
 */

#include "controller/controller_com.h"
#include "controller/controller_ring.h"
#include "controller.h"
#include "main.h"
#include "user_interface.h"
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <math.h>

struct controller_ring_buf con_com_ring_rx;

uint8_t controller_com_data[6];
int controller_com_timeout_cnt = 0;
static void Norm2Polar(analog_stick_f *stick) {
	//calc r
	stick->r = fminf(hypotf(stick->x, stick->y), 1.0f);

	//deadzone
	if (stick->r < ANALOG_STICK_DEADZONE) {
		stick->r = 0;
		stick->theta = 0;
		stick->x = stick->y = 0;
	}

	//calc theta
	if (stick->r != 0) {
		stick->theta = atan2f(stick->x,stick->y);
//		stick->theta = (180 * asinf(stick->y / stick->r)) / M_PI;
//		if (stick->x > 0 && stick->y > 0) {			//第一象限
//			;
//		} else if (stick->x < 0 && stick->y > 0) {	//第二象限
//			stick->theta = 180 - stick->theta;
//		} else if (stick->x < 0 && stick->y < 0) {	//第三象限
//			stick->theta = 180 - stick->theta;
//		} else {											//第四象限
//			stick->theta = 360 + stick->theta;
//		}
	} else {
		stick->theta = 0;
	}
}

/*
 * スティックの値を-1 ~ 1に正規化
 * @param
 * @return
 */
static float NormalizeStick(uint8_t raw_stick_data, int16_t offset) {
	return ((float)(raw_stick_data - offset) / (float)offset);
}

void controller_com_init(UART_HandleTypeDef *huart){
    controller_ring_init(&con_com_ring_rx, huart);

    HAL_UART_Receive_DMA(con_com_ring_rx.huart, con_com_ring_rx.buf, con_com_ring_rx.buf_size);
    memset(controller_com_data, 0, 6);
    HAL_Delay(10);
}

void controller_com_proc(){
    static enum controller_com_recv_st st = CONTROLLER_COM_RECV_ST_WAIT_START_0;
    uint8_t tmp[1];
    static uint8_t data_tmp[6];
    while(controller_ring_available(&con_com_ring_rx) > 0){
    	 controller_ring_getc(&con_com_ring_rx, tmp);
    	 //printf("recv:%02X\n", tmp[0]);
        switch(st){
            case CONTROLLER_COM_RECV_ST_WAIT_START_0:
            	  //printf("ST_WAIT_START_0\n");
                if(tmp[0] == CONTROLLER_COM_START_0){
                    st = CONTROLLER_COM_RECV_ST_WAIT_START_1;
                }else{
                    st = CONTROLLER_COM_RECV_ST_WAIT_START_0;
                }
                break;
            case CONTROLLER_COM_RECV_ST_WAIT_START_1:
          	  //printf("ST_WAIT_START_1\n");
                if(tmp[0] == CONTROLLER_COM_START_1){
                    st = CONTROLLER_COM_RECV_ST_WAIT_DATA_0;
                }else{
                    st = CONTROLLER_COM_RECV_ST_WAIT_START_0;
                }
                break;
            case CONTROLLER_COM_RECV_ST_WAIT_DATA_0:
          	  //printf("ST_WAIT_DATA_0\n");
            	data_tmp[0] = tmp[0];
            	  st = CONTROLLER_COM_RECV_ST_WAIT_DATA_1;
                break;
            case CONTROLLER_COM_RECV_ST_WAIT_DATA_1:
            	  //printf("ST_WAIT_DATA_1\n");
                data_tmp[1] = tmp[0];
          	  st = CONTROLLER_COM_RECV_ST_WAIT_DATA_2;
                break;
            case CONTROLLER_COM_RECV_ST_WAIT_DATA_2:
            	  //printf("ST_WAIT_DATA_2\n");
                data_tmp[2] = tmp[0];
          	  st = CONTROLLER_COM_RECV_ST_WAIT_DATA_3;
                break;
            case CONTROLLER_COM_RECV_ST_WAIT_DATA_3:
            	  //printf("ST_WAIT_DATA_3\n");
                data_tmp[3] = tmp[0];
          	  st = CONTROLLER_COM_RECV_ST_WAIT_DATA_4;
                break;
            case CONTROLLER_COM_RECV_ST_WAIT_DATA_4:
            	  //printf("ST_WAIT_DATA_4\n");
                data_tmp[4] = tmp[0];
          	  st = CONTROLLER_COM_RECV_ST_WAIT_DATA_5;
                break;
            case CONTROLLER_COM_RECV_ST_WAIT_DATA_5:
            	  //printf("ST_WAIT_DATA_5\n");
                data_tmp[5] = tmp[0];
          	  st = CONTROLLER_COM_RECV_ST_WAIT_END_0;
                break;
            case CONTROLLER_COM_RECV_ST_WAIT_END_0:
            	  //printf("ST_WAIT_END_0\n");
                if(tmp[0] == CONTROLLER_COM_END_0){
                    st = CONTROLLER_COM_RECV_ST_WAIT_END_1;
                }else{
                    st = CONTROLLER_COM_RECV_ST_WAIT_START_0;
                }
                break;
            case CONTROLLER_COM_RECV_ST_WAIT_END_1:
            	  //printf("ST_WAIT_END_1\n");
                if(tmp[0] == CONTROLLER_COM_END_1){
                    memcpy(controller_com_data, data_tmp, 6);
                    	/*
                    	 printf("Recv Controller:%02X %02X %02X %02X %02X %02X\n",
                    				controller_com_data[0], controller_com_data[1], controller_com_data[2],
								    controller_com_data[3], controller_com_data[4], controller_com_data[5]);
                       */
                    controller_com_timeout_cnt = 0;
                }
                st = CONTROLLER_COM_RECV_ST_WAIT_START_0;
                break;
            default:
                st = CONTROLLER_COM_RECV_ST_WAIT_START_0;
                break;
        }
    }

}
void controller_com_get_data(uint16_t *button , analog_stick_f *left_stick, analog_stick_f * right_stick){
	controller_com_proc();
	analog_stick left_offset, right_offset;
	//オフセット設定
	left_offset.x = 128;
	left_offset.y = 128;
	right_offset.x = 128;
	right_offset.y = 128;
	*button = ((controller_com_data[0] << 8) + controller_com_data[1]);
	if(controller_com_data[2] == 0xFF && controller_com_data[3] == 0xFF && controller_com_data[4] == 0xFF && controller_com_data[5] == 0xFF){
		//(x,y)を-1~1に正規化したもの
		left_stick->x  = 0;
		left_stick->y  = 0;
		right_stick->x = 0;
		right_stick->y = 0;
	}else{
		//(x,y)を-1~1に正規化したもの
		left_stick->x  = NormalizeStick(controller_com_data[4], left_offset.x);
		left_stick->y  = NormalizeStick(controller_com_data[5], left_offset.y)  * (-1);
		right_stick->x = NormalizeStick(controller_com_data[2], right_offset.x);
		right_stick->y = NormalizeStick(controller_com_data[3], right_offset.y) * (-1);
	}
	//極座標計算&deadzone
	Norm2Polar(left_stick);
	Norm2Polar(right_stick);
	static int cnt = 0;
	if(cnt == 10){
//		printf("Left x:%f y:%f Right x:%f y:%f\n", left_stick->x, left_stick->y, right_stick->x, right_stick->y);
		cnt = 0;
	}else{
		cnt++;
	}
}

//10ms timer
void controller_com_timer(){
    controller_com_proc();
    if(controller_com_timeout_cnt == 5){
    	printf("Timeout\n");
    	UILEDSet(0b0000);
    	failsafe=FAIL_CONNECTION;
        memset(controller_com_data, 0, 2);
        memset(controller_com_data+2, 0xFF, 4);
        controller_com_timeout_cnt++;
    }else if(controller_com_timeout_cnt < 5){
		  UILEDSet(0b1100);
        controller_com_timeout_cnt++;
    }
}

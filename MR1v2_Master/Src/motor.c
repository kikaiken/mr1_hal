/*
 * motor.c
 *
 *  Created on: 2018/11/25
 *      Author: nabeya11
 */

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "main.h"
#include "motor.h"
#include "light_math.h"
#include "user_interface.h"
#include "so1602a_i2c.h"

/*-----------------------------------------------
 * OmniWheel
 ----------------------------------------------*/
/*
 * オムニ初期化
 * @param
 * @return
 */
void undermotor_init(void){
	HAL_GPIO_WritePin(RST1_GPIO_Port,RST1_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(RST2_GPIO_Port,RST2_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(RST3_GPIO_Port,RST3_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(RST4_GPIO_Port,RST4_Pin,GPIO_PIN_SET);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_4);
}
/*
 * オムニリセット
 * @param
 * @return
 */
void undermotor_reset(void){
	HAL_GPIO_WritePin(RST1_GPIO_Port,RST1_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(RST2_GPIO_Port,RST2_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(RST3_GPIO_Port,RST3_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(RST4_GPIO_Port,RST4_Pin,GPIO_PIN_RESET);
	HAL_Delay(500);
	HAL_GPIO_WritePin(RST1_GPIO_Port,RST1_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(RST2_GPIO_Port,RST2_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(RST3_GPIO_Port,RST3_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(RST4_GPIO_Port,RST4_Pin,GPIO_PIN_SET);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_4);
}
/*
 * 個々のオムニ駆動
 * @param	undermotor_t mchannel : motor.h内に指定
 * *		float mspeed : 速度　-1~1
 * @return	uint8_t : 速度オーバーなら1
 */
uint8_t UnderMotor_Raw(undermotor_t mchannel, float mspeed){
	if(fabsf(mspeed) <= MAX_SPEED_INDI){
		switch (mchannel) {
			case TIM_CHANNEL_1:
				if(mspeed>0) HAL_GPIO_WritePin(PHASE1_GPIO_Port,PHASE1_Pin,GPIO_PIN_SET);
				else HAL_GPIO_WritePin(PHASE1_GPIO_Port,PHASE1_Pin,GPIO_PIN_RESET);
				break;
			case TIM_CHANNEL_2:
				if(mspeed>0) HAL_GPIO_WritePin(PHASE2_GPIO_Port,PHASE2_Pin,GPIO_PIN_SET);
				else HAL_GPIO_WritePin(PHASE2_GPIO_Port,PHASE2_Pin,GPIO_PIN_RESET);
				break;
			case TIM_CHANNEL_3:
				if(mspeed>0) HAL_GPIO_WritePin(PHASE3_GPIO_Port,PHASE3_Pin,GPIO_PIN_SET);
				else HAL_GPIO_WritePin(PHASE3_GPIO_Port,PHASE3_Pin,GPIO_PIN_RESET);
				break;
			case TIM_CHANNEL_4:
				if(mspeed>0) HAL_GPIO_WritePin(PHASE4_GPIO_Port,PHASE4_Pin,GPIO_PIN_SET);
				else HAL_GPIO_WritePin(PHASE4_GPIO_Port,PHASE4_Pin,GPIO_PIN_RESET);
				break;
			default:
				mspeed=0;
				break;
		}

		uint32_t mspeed_32=fabsf(mspeed*__HAL_TIM_GET_AUTORELOAD(&htim1));
		__HAL_TIM_SET_COMPARE(&htim1, mchannel, mspeed_32);
		return 0;
	}
	else{
		return 1;
	}

}

/*
 * オムニ全停止
 * @param
 * @return
 */
void UnderMotor_Stop(void){
	UnderMotor_Raw(UNDERMOTOR_FL, 0);
	UnderMotor_Raw(UNDERMOTOR_FR, 0);
	UnderMotor_Raw(UNDERMOTOR_BL, 0);
	UnderMotor_Raw(UNDERMOTOR_BR, 0);
}

/*
 * オムニx-y-omega-theta駆動
 * @param	float x_speed : 正は右方向
 * 			float y_speed : 正は前方向
 * 			float m_omega : 正は左回転
 * 			float m_theta : 正は左回転
 * @note	MAX_SPEEDで頭打ち
 * @return 個々のモータの速度超過でnone0,他で0
 */
uint8_t UnderMotor_xyot(float x_speed, float y_speed, float m_omega, float m_theta){
	uint8_t mparam_over=0;

	x_speed = fmaxf(fminf(x_speed, MAX_SPEED), -MAX_SPEED);
	y_speed = fmaxf(fminf(y_speed, MAX_SPEED), -MAX_SPEED);
	m_omega = fmaxf(fminf(m_omega, MAX_ROTATE), -MAX_ROTATE);

	mparam_over += UnderMotor_Raw(UNDERMOTOR_FL, -x_speed*cosf(m_theta+M_PI_4) - y_speed*sinf(m_theta+M_PI_4) + m_omega);
	mparam_over += UnderMotor_Raw(UNDERMOTOR_FR, -x_speed*sinf(m_theta+M_PI_4) + y_speed*cosf(m_theta+M_PI_4) + m_omega);
	mparam_over += UnderMotor_Raw(UNDERMOTOR_BL,  x_speed*sinf(m_theta+M_PI_4) - y_speed*cosf(m_theta+M_PI_4) + m_omega);
	mparam_over += UnderMotor_Raw(UNDERMOTOR_BR,  x_speed*cosf(m_theta+M_PI_4) + y_speed*sinf(m_theta+M_PI_4) + m_omega);

	if(mparam_over){
		UnderMotor_Stop();
	}

	return mparam_over;
}

/*
 * オムニx-y-omega-theta駆動 PIDつき
 * @param	struct_state* robot_state : ロボットの状態
 * 			struct_pos* ref_vel : 目標速度
 * @return 速度超過で-1,他で0
 */
failsafe_t UnderMotor_Velocity_withPID(struct_state* robot_state, struct_pos* ref_vel){
	struct_pos diff;				//a.目標との差
	uint8_t mparam_over=0;

	diff.x = ref_vel->x - robot_state->vel.vx;
	diff.y = ref_vel->y - robot_state->vel.vy;

	robot_state->vel_diff_integ.vx += diff.x;
	robot_state->vel_diff_integ.vy += diff.y;

	diff.theta = ref_vel->theta - robot_state->odometry.theta;
	//failsafe for too much difference of theta
	if (fabsf(RAD2DEG(diff.theta)) >= MAX_DIFF_THETA_REF) {
		return TOO_DIFF_FROM_REF;
	}
	robot_state->apply.x = diff.x * V_PGAIN + (robot_state->prev_vel.vx - robot_state->vel.vx) * V_DGAIN + robot_state->vel_diff_integ.vx * V_IGAIN;
	robot_state->apply.y = diff.y * V_PGAIN + (robot_state->prev_vel.vy - robot_state->vel.vy) * V_DGAIN + robot_state->vel_diff_integ.vy * V_IGAIN;
	robot_state->apply.theta = diff.theta * T_PGAIN + robot_state->angvel * (-T_DGAIN);

	mparam_over=UnderMotor_xyot(robot_state->apply.x, robot_state->apply.y, robot_state->apply.theta, robot_state->odometry.theta);

	if(mparam_over){
		return OVER_SPEED;
	}
	else{
		return NO_ERROR;
	}
}

/*
 * オムニx-y-omega-theta駆動 PIDつき
 * @param	struct_state* robot_state : ロボットの状態
 * 			struct_pos* ref_vel : 目標速度
 * @return 速度超過で-1,他で0
 */
failsafe_t UnderMotor_Velocity_withOmega(struct_state* robot_state, struct_pos* ref_vel){
	struct_pos diff;				//a.目標との差
	uint8_t mparam_over=0;

	diff.x = ref_vel->x - robot_state->vel.vx;
	diff.y = ref_vel->y - robot_state->vel.vy;
	diff.omega = ref_vel->omega - robot_state->angvel;

	robot_state->vel_diff_integ.vx += diff.x;
	robot_state->vel_diff_integ.vy += diff.y;

	robot_state->apply.x = diff.x * V_PGAIN + (robot_state->prev_vel.vx - robot_state->vel.vx) * V_DGAIN + robot_state->vel_diff_integ.vx * V_IGAIN;
	robot_state->apply.y = diff.y * V_PGAIN + (robot_state->prev_vel.vy - robot_state->vel.vy) * V_DGAIN + robot_state->vel_diff_integ.vy * V_IGAIN;
	robot_state->apply.omega = diff.omega * O_PGAIN;

	mparam_over=UnderMotor_xyot(robot_state->apply.x, robot_state->apply.y, robot_state->apply.omega, robot_state->odometry.theta);

	if(mparam_over){
		return OVER_SPEED;
	}
	else{
		return NO_ERROR;
	}
}

/*
 * オムニx-y-omega-theta駆動 PIDつき
 * @param	struct_state* robot_state : ロボットの状態
 * 			struct_pos* ref_state : 目標位置
 * @return 速度超過で-1,他で0
 */
failsafe_t UnderMotor_Location_withPID(struct_state* robot_state, struct_pos* ref_state){
	struct_pos diff;				//a.目標との差
	uint8_t mparam_over=0;

	diff.x = ref_state->x - robot_state->odometry.x;
	diff.y = ref_state->y - robot_state->odometry.y;
	//failsafe for too much difference of odometry
	if (diff.x >= MAX_DIFF_XY_REF || diff.y >= MAX_DIFF_XY_REF) {
		return TOO_DIFF_FROM_REF;
	}
	robot_state->odometry_diff_integ.x += diff.x;
	robot_state->odometry_diff_integ.y += diff.y;

	diff.theta = ref_state->theta - robot_state->odometry.theta;
	//failsafe for too much difference of theta
	if (fabsf(RAD2DEG(diff.theta)) >= 60) {
		return TOO_DIFF_FROM_REF;
	}
	robot_state->apply.x = (diff.x) * X_PGAIN + (robot_state->prev_odometry.x - robot_state->odometry.x) * X_DGAIN + robot_state->odometry_diff_integ.x * X_IGAIN;
	robot_state->apply.y = (diff.y) * X_PGAIN + (robot_state->prev_odometry.y - robot_state->odometry.y) * X_DGAIN + robot_state->odometry_diff_integ.y * X_IGAIN;
	robot_state->apply.theta = (diff.theta) * T_PGAIN + robot_state->angvel * (-T_DGAIN);

	mparam_over=UnderMotor_xyot(robot_state->apply.x, robot_state->apply.y, robot_state->apply.theta, robot_state->odometry.theta);

	if(mparam_over){
		return OVER_SPEED;
	}
	else{
		return NO_ERROR;
	}
}

void CheckOmniWheelDirection(void) {
	if(OPSwitchIsOn(SW_MODE1) == SWITCH_ON){
		OLED_Clear();
		printf("CH1 powered\n\r");
		UnderMotor_Raw(TIM_CHANNEL_1,500);
		UnderMotor_Raw(TIM_CHANNEL_2,0);
		UnderMotor_Raw(TIM_CHANNEL_3,0);
		UnderMotor_Raw(TIM_CHANNEL_4,0);
		while(OPSwitchIsOn(SW_MODE1) == SWITCH_ON);
	}
	else if(OPSwitchIsOn(SW_MODE2) == SWITCH_ON){
		OLED_Clear();
		printf("CH2 powered\n\r");
		UnderMotor_Raw(TIM_CHANNEL_1,0);
		UnderMotor_Raw(TIM_CHANNEL_2,500);
		UnderMotor_Raw(TIM_CHANNEL_3,0);
		UnderMotor_Raw(TIM_CHANNEL_4,0);
		while(OPSwitchIsOn(SW_MODE2) == SWITCH_ON);
	}
	else if(OPSwitchIsOn(SW_MODE3) == SWITCH_ON){
		OLED_Clear();
		printf("CH3 powered\n\r");
		UnderMotor_Raw(TIM_CHANNEL_1,0);
		UnderMotor_Raw(TIM_CHANNEL_2,0);
		UnderMotor_Raw(TIM_CHANNEL_3,500);
		UnderMotor_Raw(TIM_CHANNEL_4,0);
		while(OPSwitchIsOn(SW_MODE3) == SWITCH_ON);
	}
	else if(OPSwitchIsOn(SW_MODE4) == SWITCH_ON){
		OLED_Clear();
		printf("CH4 powered\n\r");
		UnderMotor_Raw(TIM_CHANNEL_1,0);
		UnderMotor_Raw(TIM_CHANNEL_2,0);
		UnderMotor_Raw(TIM_CHANNEL_3,0);
		UnderMotor_Raw(TIM_CHANNEL_4,500);
		while(OPSwitchIsOn(SW_MODE4) == SWITCH_ON);
	}
}

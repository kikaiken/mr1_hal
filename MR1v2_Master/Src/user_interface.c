/*
 * user_interface.cpp
 *
 *  Created on: 2018/05/20
 *      Author: Ryohei
 */


#include "user_interface.h"
#include "failsafe.h"

/*-----------------------------------------------
 * LED・ブザー表示パターン
 ----------------------------------------------*/
/*
 * 警告表示LED
 * @param
 * @return
 */
void UIWarning(void) {
	for (uint8_t i = 0; i < 3; i++) {
		OPLEDSet(0b1111);
		HAL_Delay(200);
		BuzzerBeep(BUZZER_A6, 200);
		OPLEDSet(0);
		HAL_Delay(50);
	}
}

/*-----------------------------------------------
 * 操作盤
 ----------------------------------------------*/
/*
 * 操作盤上のLED
 * @param	led : LEDを2進数の各ビットに見立てた時の値(ex:OPLEDSet(0b1010))
 * @return
 */
void OPLEDSet(int8_t led) {
	HAL_GPIO_WritePin(MODE1LD_GPIO_Port,MODE1LD_Pin, ((led & 0b00000001) ? (GPIO_PIN_RESET) : (GPIO_PIN_SET)));
	HAL_GPIO_WritePin(MODE2LD_GPIO_Port,MODE2LD_Pin, ((led & 0b00000010) ? (GPIO_PIN_RESET) : (GPIO_PIN_SET)));
	HAL_GPIO_WritePin(MODE3LD_GPIO_Port,MODE3LD_Pin, ((led & 0b00000100) ? (GPIO_PIN_RESET) : (GPIO_PIN_SET)));
	HAL_GPIO_WritePin(MODE4LD_GPIO_Port,MODE4LD_Pin, ((led & 0b00001000) ? (GPIO_PIN_RESET) : (GPIO_PIN_SET)));
}

/*
 * 操作盤のスイッチ
 * @param	button_num : ボタンの番号
 * @return	SWITCH_ON/SWITCH_OFF
 */
switchstate_t OPSwitchIsOn(switchnum_t switch_num) {
	switchstate_t switchstate=SWITCH_OFF;
	switch(switch_num){
		//でかいスタートスイッチ
		case SW_START:
			switchstate = HAL_GPIO_ReadPin(STARTSW_GPIO_Port, STARTSW_Pin) == GPIO_PIN_RESET ? SWITCH_ON : SWITCH_OFF;
			break;

		//プッシュスイッチ
		case SW_MODE1:
			switchstate = HAL_GPIO_ReadPin(MODE1SW_GPIO_Port, MODE1SW_Pin) == GPIO_PIN_SET ? SWITCH_ON : SWITCH_OFF;
			break;
		case SW_MODE2:
			switchstate = HAL_GPIO_ReadPin(MODE2SW_GPIO_Port, MODE2SW_Pin) == GPIO_PIN_SET ? SWITCH_ON : SWITCH_OFF;
			break;
		case SW_MODE3:
			switchstate = HAL_GPIO_ReadPin(MODE3SW_GPIO_Port, MODE3SW_Pin) == GPIO_PIN_SET ? SWITCH_ON : SWITCH_OFF;
			break;
		case SW_MODE4:
			switchstate = HAL_GPIO_ReadPin(MODE4SW_GPIO_Port, MODE4SW_Pin) == GPIO_PIN_SET ? SWITCH_ON : SWITCH_OFF;
			break;
		case SW_F1:
			switchstate = HAL_GPIO_ReadPin(F1SW_GPIO_Port,    F1SW_Pin)    == GPIO_PIN_SET ? SWITCH_ON : SWITCH_OFF;
			break;

		//トグルスイッチ
		case SW_TOGGLE1 :
			switchstate = HAL_GPIO_ReadPin(TOGGLE1_GPIO_Port, TOGGLE1_Pin) == GPIO_PIN_SET ? SWITCH_ON : SWITCH_OFF;
			break;
		case SW_TOGGLE2 :
			switchstate = HAL_GPIO_ReadPin(TOGGLE2_GPIO_Port, TOGGLE2_Pin) == GPIO_PIN_SET ? SWITCH_ON : SWITCH_OFF;
			break;
		case SW_TOGGLE3 :
			switchstate = HAL_GPIO_ReadPin(TOGGLE3_GPIO_Port, TOGGLE3_Pin) == GPIO_PIN_SET ? SWITCH_ON : SWITCH_OFF;
			break;
		case SW_TOGGLE4 :
			switchstate = HAL_GPIO_ReadPin(TOGGLE4_GPIO_Port, TOGGLE4_Pin) == GPIO_PIN_SET ? SWITCH_ON : SWITCH_OFF;
			break;

		//ボタン番号指定間違い
		default:
			return SWITCH_NUM_ERROR;
			break;
	}
	return switchstate;
}

void OPWaitSwitchOff(switchnum_t switch_num) {
	const int16_t wait_time = 50;		//チャタリング回避用待ち時間
	HAL_Delay(wait_time);
	while (OPSwitchIsOn(switch_num) == SWITCH_ON);
//	HAL_Delay(wait_time);
//	while (OPSwitchIsOn(switch_num) == SWITCH_ON);
}

/*-----------------------------------------------
 * メイン基板UI
 ----------------------------------------------*/

uint8_t melody_select;
uint8_t melody_target;

typedef struct{
	int16_t pitch;
	int32_t tim;
}struct_melody;

uint8_t melodyend[25] = {8, 23, 13, 23, 20, 17, 17, 13, 9, 18, 12, 13, 10, 10, 10, 11};

struct_melody melody[25][25] = {
	{
		//0.ハンガリー舞曲
		{BUZZER_D6, ONE_4+ONE_8},
		{BUZZER_G6, ONE_8},
		{BUZZER_Bm6,ONE_4+ONE_8},
		{BUZZER_G6, ONE_8},
		{BUZZER_FM6,ONE_4+ONE_8},
		{BUZZER_G6, ONE_16},
		{BUZZER_A6, ONE_16},
		{BUZZER_G6, ONE_2},
	},
	{
		//1.風呂焚き
		{BUZZER_G6, ONE_8},
		{BUZZER_F6, ONE_8},
		{BUZZER_E6, ONE_4},
		{BUZZER_G6, ONE_8},
		{BUZZER_C7, ONE_8},
		{BUZZER_B6, ONE_4},
		{BUZZER_G6, ONE_8},
		{BUZZER_D7, ONE_8},
		{BUZZER_C7, ONE_4},
		{BUZZER_E7, ONE_4},
		{BUZZER_NO, ONE_4},
		{BUZZER_C7, ONE_8},
		{BUZZER_B6, ONE_8},
		{BUZZER_A6, ONE_4},
		{BUZZER_F7, ONE_8},
		{BUZZER_D7, ONE_8},
		{BUZZER_C7, ONE_4},
		{BUZZER_B6, ONE_4},
		{BUZZER_C7, ONE_8},
		{BUZZER_G6, ONE_8},
		{BUZZER_G6, ONE_8},
		{BUZZER_F6, ONE_8},
		{BUZZER_E6, ONE_4},
	},
	{
		//2.ファミマ
		{BUZZER_FM6,ONE_8},
		{BUZZER_D6, ONE_8},
		{BUZZER_A5, ONE_8},
		{BUZZER_D6, ONE_8},
		{BUZZER_E6, ONE_8},
		{BUZZER_A6, ONE_8},
		{BUZZER_NO, ONE_8},
		{BUZZER_A5, ONE_8},
		{BUZZER_E6, ONE_8},
		{BUZZER_FM6,ONE_8},
		{BUZZER_E6, ONE_8},
		{BUZZER_A5, ONE_8},
		{BUZZER_D6, ONE_8},
	},
	{
		//3.四季　春
		{BUZZER_E6, ONE_8},
		{BUZZER_GM6,ONE_8},
		{BUZZER_GM6,ONE_8},
		{BUZZER_GM6,ONE_8},
		{BUZZER_FM6,ONE_16},
		{BUZZER_E6, ONE_16},
		{BUZZER_B6, ONE_4+ONE_8},

		{BUZZER_B6, ONE_16},
		{BUZZER_A6, ONE_16},
		{BUZZER_GM6,ONE_8},
		{BUZZER_GM6,ONE_8},
		{BUZZER_GM6,ONE_8},
		{BUZZER_FM6,ONE_16},
		{BUZZER_E6, ONE_16},
		{BUZZER_B6, ONE_4+ONE_8},

		{BUZZER_B6, ONE_16},
		{BUZZER_A6, ONE_16},
		{BUZZER_GM6,ONE_8},
		{BUZZER_A6, ONE_16},
		{BUZZER_B6, ONE_16},
		{BUZZER_A6, ONE_8},
		{BUZZER_GM6,ONE_8},
		{BUZZER_FM6,ONE_2},
	},
	{
		//4.「不協和音」
		{BUZZER_D6, ONE_8},
		{BUZZER_E6, ONE_8},
		{BUZZER_F6, ONE_8},
		{BUZZER_E6, ONE_8},
		{BUZZER_D6, ONE_8},
		{BUZZER_E6, ONE_16},
		{BUZZER_F6, ONE_16},
		{BUZZER_NO, ONE_16},
		{BUZZER_F6, ONE_16},
		{BUZZER_E6, ONE_8},
		{BUZZER_D6, ONE_8},
		{BUZZER_E6, ONE_8},
		{BUZZER_F6, ONE_8},
		{BUZZER_D6, ONE_8},
		{BUZZER_G6, ONE_8},
		{BUZZER_G6, ONE_16},
		{BUZZER_G6, ONE_8},
		{BUZZER_F6, ONE_16},
		{BUZZER_E6, ONE_8},
		{BUZZER_D6, ONE_2},
	},
	{
		//5.サイレントマジョリティー
		{BUZZER_Em6, ONE_8},
		{BUZZER_F6, ONE_8},
		{BUZZER_G6, ONE_8},
		{BUZZER_Am6, ONE_8},
		{BUZZER_Am6, ONE_16},
		{BUZZER_Am6, ONE_8},
		{BUZZER_Am6, ONE_16},
		{BUZZER_Em6, ONE_8},
		{BUZZER_NO, ONE_8},
		{BUZZER_Em6, ONE_8},
		{BUZZER_Dm7, ONE_8},
		{BUZZER_C7, ONE_8},
		{BUZZER_Bm6, ONE_8},
		{BUZZER_Bm6, ONE_16},
		{BUZZER_Bm6, ONE_8},
		{BUZZER_Bm6, ONE_16},
		{BUZZER_Am6, ONE_8},
	},
	{
		//6.Chocolate
		{BUZZER_Bm6, ONE_8},
		{BUZZER_G6, ONE_8},
		{BUZZER_Bm6, ONE_8},
		{BUZZER_G6, ONE_8},
		{BUZZER_Bm6, ONE_8},
		{BUZZER_C7, ONE_8},
		{BUZZER_Em6, ONE_8},
		{BUZZER_NO, ONE_8},
		{BUZZER_Em6, ONE_8},
		{BUZZER_C6, ONE_8},
		{BUZZER_Em6, ONE_8},
		{BUZZER_C6, ONE_8},
		{BUZZER_Em6, ONE_8},
		{BUZZER_NO, ONE_8},
		{BUZZER_C7, ONE_8},
		{BUZZER_Bm6, ONE_8},
		{BUZZER_Em6, ONE_8},
	},
	{
		//7.Hankyu
		{BUZZER_Dm6, ONE_16},
		{BUZZER_Am5, ONE_16},
		{BUZZER_C6, ONE_16},
		{BUZZER_Dm6, ONE_16},
		{BUZZER_F6, ONE_16},
		{BUZZER_Em6, ONE_16},
		{BUZZER_F6, ONE_16},
		{BUZZER_G6, ONE_16},
		{BUZZER_Am6, ONE_16},
		{BUZZER_G6, ONE_16},
		{BUZZER_Am6, ONE_16},
		{BUZZER_F6, ONE_16},
		{BUZZER_Em6, ONE_8},
	},
	{
		//8.Subway
		{BUZZER_C5, ONE_8},
		{BUZZER_E5, ONE_8},
		{BUZZER_G5, ONE_8},
		{BUZZER_C6, ONE_8},
		{BUZZER_D6, ONE_8},
		{BUZZER_C6, ONE_8},
		{BUZZER_B5, ONE_8},
		{BUZZER_D6, ONE_8},
		{BUZZER_C6, ONE_8},
	},
	{
		//9.さくらんぼ
		{BUZZER_E6, ONE_8},
		{BUZZER_G6, ONE_8},
		{BUZZER_FM6, ONE_16},
		{BUZZER_G6, ONE_16},
		{BUZZER_A6, ONE_16},
		{BUZZER_D6, ONE_16},
		{BUZZER_B6, ONE_8},
		{BUZZER_C7, ONE_8},
		{BUZZER_B6, ONE_8},
		{BUZZER_A6, ONE_8},
		{BUZZER_E6, ONE_8},
		{BUZZER_FM6, ONE_16},
		{BUZZER_G6, ONE_16},
		{BUZZER_FM6, ONE_16},
		{BUZZER_G6, ONE_16},
		{BUZZER_A6, ONE_16},
		{BUZZER_FM6, ONE_16},
		{BUZZER_G6, ONE_8},
	},
	{
		//10.トトロ
		{BUZZER_F6, ONE_8},
		{BUZZER_G6, ONE_8},
		{BUZZER_A6, ONE_8},
		{BUZZER_Bm6, ONE_8},
		{BUZZER_C7, ONE_4},
		{BUZZER_A6, ONE_8},
		{BUZZER_F6, ONE_8},
		{BUZZER_NO, ONE_8},
		{BUZZER_C7, ONE_8},
		{BUZZER_NO, ONE_8},
		{BUZZER_Bm6, ONE_4},
		{BUZZER_G6, ONE_8},
	},
	{
		//11.KyotoSta.
		{BUZZER_FM6, ONE_4/3},
		{BUZZER_CM6, ONE_4/3},
		{BUZZER_AM5, ONE_4/3},
		{BUZZER_FM6, ONE_4},
		{BUZZER_GM6, ONE_4/3},
		{BUZZER_Em6, ONE_4/3},
		{BUZZER_B5,  ONE_4/3},
		{BUZZER_GM6, ONE_4},
		{BUZZER_AM6, ONE_4/3},
		{BUZZER_E6,  ONE_4/3},
		{BUZZER_CM6, ONE_4/3},
		{BUZZER_AM6, ONE_4},
		{BUZZER_B6,  ONE_2},
	},
	{
		//12.OsakaMetro
		{BUZZER_D6, ONE_8},
		{BUZZER_A5, ONE_8},
		{BUZZER_E6, ONE_8},
		{BUZZER_A5, ONE_8},
		{BUZZER_FM6,ONE_4},
		{BUZZER_D6, ONE_8},
		{BUZZER_A5, ONE_8},
		{BUZZER_E6, ONE_8},
		{BUZZER_A5, ONE_8},
		{BUZZER_D6, ONE_4},
	},
	{
		//13.ProjectX
		{BUZZER_D6, ONE_8+ONE_16},
		{BUZZER_E6, ONE_8+ONE_16},
		{BUZZER_F6, ONE_8},
		{BUZZER_E6, ONE_4},
		{BUZZER_NO, ONE_8},
		{BUZZER_A5, ONE_8},
		{BUZZER_E6, ONE_8+ONE_16},
		{BUZZER_F6, ONE_8+ONE_16},
		{BUZZER_E6, ONE_8},
		{BUZZER_D6, ONE_4},
	},
	{
		//14.もののけ
		{BUZZER_C6, ONE_8},
		{BUZZER_F6, ONE_8},
		{BUZZER_F6, ONE_8},
		{BUZZER_Em6, ONE_8},
		{BUZZER_F6, ONE_4},
		{BUZZER_NO, ONE_8},
		{BUZZER_F6, ONE_8},
		{BUZZER_Am6, ONE_8},
		{BUZZER_Em6, ONE_8},
		{BUZZER_Em6, ONE_4},
	},
	{
		//15.ポニョ
		{BUZZER_C7, ONE_4},
		{BUZZER_A6, ONE_8},
		{BUZZER_F6, ONE_4},
		{BUZZER_C6, ONE_8},
		{BUZZER_C6, ONE_8},
		{BUZZER_C6, ONE_8},
		{BUZZER_D6, ONE_8},
		{BUZZER_F6, ONE_8},
		{BUZZER_Bm6, ONE_8},
		{BUZZER_D7, ONE_8},
		{BUZZER_C7, ONE_8},
	}
};

void BuzzerUpdate(int16_t freq);
/*
 * メロディーを鳴らす（割り込み式）
 */
void Melody_Start(uint8_t value){
	melody_select=value;
	melody_target = 0;
	BuzzerOff();
	HAL_TIM_Base_Stop_IT(&TIM_MELODY_HANDLER);

	__HAL_TIM_SET_COUNTER(&TIM_MELODY_HANDLER, 0);
	__HAL_TIM_SET_AUTORELOAD(&TIM_MELODY_HANDLER, melody[melody_select][melody_target].tim*1000);
	BuzzerOn(melody[melody_select][melody_target].pitch);
	melody_target++;
	HAL_TIM_Base_Start_IT(&TIM_MELODY_HANDLER);
}

/*
 * メロディー更新
 * 割り込み式に必須
 * タイマ割り込み内で実行
 */
uint8_t Melody_Update(void){
	if(melody_target < melodyend[melody_select]){
		BuzzerUpdate(melody[melody_select][melody_target].pitch);
		__HAL_TIM_SET_AUTORELOAD(&TIM_MELODY_HANDLER, melody[melody_select][melody_target].tim*1000);
		melody_target++;
		return 0;
	}
	else{
		BuzzerOff();
		HAL_TIM_Base_Stop_IT(&TIM_MELODY_HANDLER);
		return 1;
	}
}

/*
 * メロディーを鳴らす
 * @param	value : メロディーの種類
 * @return
 */
void Melody(uint8_t value){
	switch(value){
		case FINISH_PROCESS:
			//愛の挨拶
			BuzzerBeep(BUZZER_FM6,ONE_4);
			BuzzerBeep(BUZZER_A5,ONE_8);
			BuzzerBeep(BUZZER_FM6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_CM6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_G6,ONE_4);
			BuzzerBeep(BUZZER_G6,ONE_4);
			BuzzerBeep(BUZZER_G6,ONE_4);
			BuzzerBeep(BUZZER_A5,ONE_4);

			BuzzerBeep(BUZZER_FM6,ONE_4);
			BuzzerBeep(BUZZER_AM5,ONE_8);
			BuzzerBeep(BUZZER_FM6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_CM6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_4);
			BuzzerBeep(BUZZER_E6,ONE_4);
			BuzzerBeep(BUZZER_E6,ONE_4);
			BuzzerBeep(BUZZER_F6,ONE_4);

			BuzzerBeep(BUZZER_FM6,ONE_4);
			BuzzerBeep(BUZZER_A5,ONE_8);
			BuzzerBeep(BUZZER_FM6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_CM6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_B6,ONE_4);
			BuzzerBeep(BUZZER_B6,ONE_4);
			BuzzerBeep(BUZZER_B6,ONE_4);

			BuzzerBeep(BUZZER_A6,ONE_8);
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_FM6,ONE_4);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_B5,ONE_4);
			BuzzerBeep(BUZZER_CM6,ONE_4);
			BuzzerBeep(BUZZER_D6,ONE_2);
			break;
		case CANCEL_PROCESS:
			//マクド
			for(int i=0;i<6;i++){
				BuzzerBeep(BUZZER_G5,ONE_16);
				HAL_Delay(ONE_16+ONE_8);
				BuzzerBeep(BUZZER_G5,ONE_8);
				BuzzerBeep(BUZZER_F5,ONE_8);
			}
			break;
		case TOO_DIFF_FROM_REF:
			//運命
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_2+ONE_4);
			BuzzerBeep(BUZZER_F6,ONE_8);
			BuzzerBeep(BUZZER_F6,ONE_8);
			BuzzerBeep(BUZZER_F6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_2+ONE_4);
			break;
		case OVER_SPEED:
			//フーガ
			BuzzerBeep(BUZZER_A6,ONE_8);
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_A6,ONE_2+ONE_4);
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_F6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_CM6,ONE_4);
			BuzzerBeep(BUZZER_D6,ONE_2);
			break;
		case NO_MODE:
			//チャルメラ
			BuzzerBeep(BUZZER_C6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_4+ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_C6,ONE_8);
			HAL_Delay(ONE_8);
			BuzzerBeep(BUZZER_C6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_C6,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8+ONE_2);
			break;
		case FAIL_CONNECTION:
			//横断歩道？
			for(int i=0;i<4;i++){
				BuzzerBeep(BUZZER_G6,ONE_16);
				HAL_Delay(ONE_16);
				BuzzerBeep(BUZZER_E6,ONE_8);
				HAL_Delay(ONE_8);
				BuzzerBeep(BUZZER_G6,ONE_32);
				HAL_Delay(ONE_32);
				BuzzerBeep(BUZZER_G6,ONE_32);
				HAL_Delay(ONE_32);
				BuzzerBeep(BUZZER_E6,ONE_8);
				HAL_Delay(ONE_8);
			}
			break;
		case UNKOWN_ERROR:
			break;
		default:
			break;
	}
}

/*
 * アルペジオ
 * @param	value : アルペジオの種類
 * @return
 */
void Arpeggio(uint8_t value){
	switch(value){
		case 0:
			BuzzerBeep(BUZZER_C5,ONE_8);
			BuzzerBeep(BUZZER_E5,ONE_8);
			BuzzerBeep(BUZZER_G5,ONE_8);
			break;
		case 1:
			BuzzerBeep(BUZZER_D5,ONE_8);
			BuzzerBeep(BUZZER_FM5,ONE_8);
			BuzzerBeep(BUZZER_A5,ONE_8);
			break;
		case 2:
			BuzzerBeep(BUZZER_E5,ONE_8);
			BuzzerBeep(BUZZER_GM5,ONE_8);
			BuzzerBeep(BUZZER_B5,ONE_8);
			break;
		case 3:
			BuzzerBeep(BUZZER_F5,ONE_8);
			BuzzerBeep(BUZZER_A5,ONE_8);
			BuzzerBeep(BUZZER_C6,ONE_8);
			break;
		case 4:
			BuzzerBeep(BUZZER_G5,ONE_8);
			BuzzerBeep(BUZZER_B5,ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_8);
			break;
		case 5:
			BuzzerBeep(BUZZER_D6,ONE_16);
			BuzzerBeep(BUZZER_D6,ONE_16);
			HAL_Delay(ONE_8);
			BuzzerBeep(BUZZER_D6,ONE_16);
			BuzzerBeep(BUZZER_D7,ONE_4);
			break;
		case 6:
			BuzzerBeep(BUZZER_D6,ONE_8);
			BuzzerBeep(BUZZER_FM6,ONE_8);
			BuzzerBeep(BUZZER_A6,ONE_8);
			BuzzerBeep(BUZZER_B6,ONE_8);
			HAL_Delay(ONE_8);
			BuzzerBeep(BUZZER_B6,ONE_8);
			BuzzerBeep(BUZZER_CM7,ONE_8);
			BuzzerBeep(BUZZER_B6,ONE_8);
			BuzzerBeep(BUZZER_A6,ONE_8);
			HAL_Delay(ONE_8);
			BuzzerBeep(BUZZER_FM6,ONE_4);
			break;
		case 7:
			BuzzerBeep(BUZZER_E6,ONE_8);
			BuzzerBeep(BUZZER_C6,ONE_8);
			HAL_Delay(ONE_8);
			BuzzerBeep(BUZZER_G6,ONE_8);
			BuzzerBeep(BUZZER_C6,ONE_8);
			break;
		default:
			break;
	}
}

/*
 * ブザーを指定された周波数で指定された時間鳴らす
 * @param	freq : 周波数(Hz)
 * 			time : 時間(ms)
 * 			(ex:BuzzerBeep(BUZZER_A7, 1000))
 * @return
 */
void BuzzerBeep(int16_t freq, int32_t time) {
	BuzzerOn(freq);
	HAL_Delay(time);
	BuzzerOff();
}

/*
 * ブザーを指定された周波数で鳴らす
 * @param
 * @return
 */
void BuzzerOn(int16_t freq) {
//	BUZZER_TIM_HANDLER.Init.Period = (float)BUZZER_TIM_FREQ / (float)freq;
//	HAL_TIM_Base_Init(&BUZZER_TIM_HANDLER);
//	HAL_TIM_PWM_Init(&BUZZER_TIM_HANDLER);
//	__HAL_TIM_SetCompare(&BUZZER_TIM_HANDLER, BUZZER_TIM_CHANNEL, (uint16_t)((float)BUZZER_TIM_FREQ / ((float)freq * 2.0f)));

	//PWMスタート
	BuzzerUpdate(freq);
	HAL_TIM_PWM_Start(&BUZZER_TIM_HANDLER, BUZZER_TIM_CHANNEL);
}

/*
 * ブザー止める
 * @param
 * @return
 */
void BuzzerOff(void) {
	HAL_TIM_PWM_Stop(&BUZZER_TIM_HANDLER, BUZZER_TIM_CHANNEL);
}

/*
 * ブザーを指定された周波数で更新
 * @param
 * @return
 */
void BuzzerUpdate(int16_t freq) {
	__HAL_TIM_SetCounter(&BUZZER_TIM_HANDLER, 0);
	__HAL_TIM_SET_AUTORELOAD(&BUZZER_TIM_HANDLER, (uint16_t)((float)BUZZER_TIM_FREQ / (float)freq));
	__HAL_TIM_SetCompare(&BUZZER_TIM_HANDLER, BUZZER_TIM_CHANNEL, (uint16_t)((float)BUZZER_TIM_FREQ / ((float)freq * 2.0f)));
}

/*
 * 指定にしたがってUILEDを点灯
 * @param	led : LEDを2進数の各ビットに見立てた時の値(ex:UILEDSet(0b1010))
 * @return
 */
void UILEDSet(uint8_t led){
	HAL_GPIO_WritePin(UILED_BIT1_PORT, UILED_BIT1_PIN, ((led & 0b00000001) ? (GPIO_PIN_SET) : (GPIO_PIN_RESET)));
	HAL_GPIO_WritePin(UILED_BIT2_PORT, UILED_BIT2_PIN, ((led & 0b00000010) ? (GPIO_PIN_SET) : (GPIO_PIN_RESET)));
	HAL_GPIO_WritePin(UILED_BIT3_PORT, UILED_BIT3_PIN, ((led & 0b00000100) ? (GPIO_PIN_SET) : (GPIO_PIN_RESET)));
	HAL_GPIO_WritePin(UILED_BIT4_PORT, UILED_BIT4_PIN, ((led & 0b00001000) ? (GPIO_PIN_SET) : (GPIO_PIN_RESET)));
}

/*
 * ボタンのON/OFFを返す
 * @param	button_num : ボタンの番号1~3
 * @return	PUSHSW_ON/PUSHSW_OFF
 */
buttonstate_t ButtonIsOn(uint8_t button_num) {
	//存在しないボタンを指定している場合
	if (button_num > NUM_OF_BUTTON) {
		return BUTTON_NUM_ERROR;
	}

	if (button_num == 1) {
#ifdef BUTTON_1_PULL_UP
		if (HAL_GPIO_ReadPin(BUTTON_1_PORT, BUTTON_1_PIN) == GPIO_PIN_SET) {return BUTTON_OFF;}
		else 															   {return BUTTON_ON;}
#endif
#ifdef BUTTON_1_PULL_DOWN
		if (HAL_GPIO_ReadPin(BUTTON1_GPIO_Port, BUTTON1_Pin) == GPIO_PIN_SET) {return BUTTON_ON;}
		else 															   {return BUTTON_OFF;}
#endif
	} else if (button_num == 2) {
#ifdef BUTTON_2_PULL_UP
		if (HAL_GPIO_ReadPin(BUTTON_2_PORT, BUTTON_2_PIN) == GPIO_PIN_SET) {return BUTTON_OFF;}
		else 															   {return BUTTON_ON;}
#endif
#ifdef BUTTON_2_PULL_DOWN
		if (HAL_GPIO_ReadPin(BUTTON2_GPIO_Port, BUTTON2_Pin) == GPIO_PIN_SET) {return BUTTON_ON;}
		else 															   {return BUTTON_OFF;}
#endif
	} else if (button_num == 3) {
#ifdef BUTTON_3_PULL_UP
		if (HAL_GPIO_ReadPin(BUTTON_3_PORT, BUTTON_3_PIN) == GPIO_PIN_SET) {return BUTTON_OFF;}
		else 															   {return BUTTON_ON;}
#endif
#ifdef BUTTON_3_PULL_DOWN
		if (HAL_GPIO_ReadPin(BUTTON3_GPIO_Port, BUTTON3_Pin) == GPIO_PIN_SET) {return BUTTON_ON;}
		else 															   {return BUTTON_OFF;}
#endif
	}

	//ここには来ないハズ
	else {
		return BUTTON_NUM_ERROR;
	}
}

/*
 * ボタンがOFFになるまで待って、長押し/短押しを返す
 * @param	button_num : ボタンの番号0~2
 * 			long_short_thres : 長押し/短押しの閾値(ms)
 * @return	BUTTON_LONG/BUTTON_SHORT
 */
int8_t ButtonJudgePushTime(uint8_t button_num, uint32_t long_short_thres) {
	//存在しないボタンを指定している場合
	if (button_num > NUM_OF_BUTTON - 1) {
		return BUTTON_NUM_ERROR;
	}

	volatile uint32_t push_time = 50;				//押し時間タイマ
	HAL_Delay(push_time);							//チャタリング防止
	while (ButtonIsOn(button_num) == BUTTON_ON) {	//ボタンが押されている間カウント
		push_time++;
		HAL_Delay(1);
	}
	if (push_time > long_short_thres) return BUTTON_LONG;
	else							  return BUTTON_SHORT;
}

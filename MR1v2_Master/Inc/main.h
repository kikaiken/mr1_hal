/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2019 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "uart_com.h"
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

void HAL_TIM_MspPostInit(TIM_HandleTypeDef *htim);

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define CS3_Pin GPIO_PIN_3
#define CS3_GPIO_Port GPIOE
#define TOGGLE2_Pin GPIO_PIN_6
#define TOGGLE2_GPIO_Port GPIOE
#define USER_Btn_Pin GPIO_PIN_13
#define USER_Btn_GPIO_Port GPIOC
#define PHASE2_Pin GPIO_PIN_3
#define PHASE2_GPIO_Port GPIOF
#define F1LD_Pin GPIO_PIN_5
#define F1LD_GPIO_Port GPIOF
#define MCO_Pin GPIO_PIN_0
#define MCO_GPIO_Port GPIOH
#define LIMIT2_Pin GPIO_PIN_0
#define LIMIT2_GPIO_Port GPIOC
#define RMII_MDC_Pin GPIO_PIN_1
#define RMII_MDC_GPIO_Port GPIOC
#define LIMIT1_Pin GPIO_PIN_3
#define LIMIT1_GPIO_Port GPIOC
#define RMII_REF_CLK_Pin GPIO_PIN_1
#define RMII_REF_CLK_GPIO_Port GPIOA
#define RMII_MDIO_Pin GPIO_PIN_2
#define RMII_MDIO_GPIO_Port GPIOA
#define MODE2SW_Pin GPIO_PIN_5
#define MODE2SW_GPIO_Port GPIOA
#define MODE3LD_Pin GPIO_PIN_6
#define MODE3LD_GPIO_Port GPIOA
#define RMII_CRS_DV_Pin GPIO_PIN_7
#define RMII_CRS_DV_GPIO_Port GPIOA
#define RMII_RXD0_Pin GPIO_PIN_4
#define RMII_RXD0_GPIO_Port GPIOC
#define RMII_RXD1_Pin GPIO_PIN_5
#define RMII_RXD1_GPIO_Port GPIOC
#define MODE4SW_Pin GPIO_PIN_1
#define MODE4SW_GPIO_Port GPIOB
#define MODE3SW_Pin GPIO_PIN_2
#define MODE3SW_GPIO_Port GPIOB
#define STARTLD_Pin GPIO_PIN_11
#define STARTLD_GPIO_Port GPIOF
#define PHASE3_Pin GPIO_PIN_14
#define PHASE3_GPIO_Port GPIOF
#define RST2_Pin GPIO_PIN_15
#define RST2_GPIO_Port GPIOF
#define MODE2LD_Pin GPIO_PIN_0
#define MODE2LD_GPIO_Port GPIOG
#define STARTSW_Pin GPIO_PIN_7
#define STARTSW_GPIO_Port GPIOE
#define PHASE4_Pin GPIO_PIN_10
#define PHASE4_GPIO_Port GPIOE
#define RST4_Pin GPIO_PIN_12
#define RST4_GPIO_Port GPIOE
#define RST3_Pin GPIO_PIN_15
#define RST3_GPIO_Port GPIOE
#define INB7_Pin GPIO_PIN_11
#define INB7_GPIO_Port GPIOB
#define INA7_Pin GPIO_PIN_12
#define INA7_GPIO_Port GPIOB
#define RMII_TXD1_Pin GPIO_PIN_13
#define RMII_TXD1_GPIO_Port GPIOB
#define F1SW_Pin GPIO_PIN_14
#define F1SW_GPIO_Port GPIOB
#define MODE4LD_Pin GPIO_PIN_15
#define MODE4LD_GPIO_Port GPIOB
#define STLK_RX_Pin GPIO_PIN_8
#define STLK_RX_GPIO_Port GPIOD
#define STLK_TX_Pin GPIO_PIN_9
#define STLK_TX_GPIO_Port GPIOD
#define PHASE1_Pin GPIO_PIN_14
#define PHASE1_GPIO_Port GPIOD
#define RST1_Pin GPIO_PIN_15
#define RST1_GPIO_Port GPIOD
#define BUTTON3_Pin GPIO_PIN_4
#define BUTTON3_GPIO_Port GPIOG
#define LED2_Pin GPIO_PIN_5
#define LED2_GPIO_Port GPIOG
#define LED1_Pin GPIO_PIN_6
#define LED1_GPIO_Port GPIOG
#define BUTTON2_Pin GPIO_PIN_7
#define BUTTON2_GPIO_Port GPIOG
#define LED3_Pin GPIO_PIN_8
#define LED3_GPIO_Port GPIOG
#define INB6_Pin GPIO_PIN_8
#define INB6_GPIO_Port GPIOA
#define INB5_Pin GPIO_PIN_11
#define INB5_GPIO_Port GPIOA
#define INA5_Pin GPIO_PIN_12
#define INA5_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define BUTTON1_Pin GPIO_PIN_15
#define BUTTON1_GPIO_Port GPIOA
#define CS1_Pin GPIO_PIN_2
#define CS1_GPIO_Port GPIOD
#define CS2_Pin GPIO_PIN_7
#define CS2_GPIO_Port GPIOD
#define TOGGLE3_Pin GPIO_PIN_9
#define TOGGLE3_GPIO_Port GPIOG
#define MODE1SW_Pin GPIO_PIN_10
#define MODE1SW_GPIO_Port GPIOG
#define RMII_TX_EN_Pin GPIO_PIN_11
#define RMII_TX_EN_GPIO_Port GPIOG
#define MODE1LD_Pin GPIO_PIN_12
#define MODE1LD_GPIO_Port GPIOG
#define RMII_TXD0_Pin GPIO_PIN_13
#define RMII_TXD0_GPIO_Port GPIOG
#define TOGGLE4_Pin GPIO_PIN_15
#define TOGGLE4_GPIO_Port GPIOG
#define INA6_Pin GPIO_PIN_6
#define INA6_GPIO_Port GPIOB
#define LD2_Pin GPIO_PIN_7
#define LD2_GPIO_Port GPIOB
#define INA8_Pin GPIO_PIN_8
#define INA8_GPIO_Port GPIOB
#define INB8_Pin GPIO_PIN_9
#define INB8_GPIO_Port GPIOB
#define LED4_Pin GPIO_PIN_0
#define LED4_GPIO_Port GPIOE
#define TOGGLE1_Pin GPIO_PIN_1
#define TOGGLE1_GPIO_Port GPIOE
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

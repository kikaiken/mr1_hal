/*
 * controller.h
 *
 *  Created on: 2018/06/15
 *      Author: Ryohei
 */

#ifndef CONTROLLER_H_
#define CONTROLLER_H_

#include "stm32f7xx_hal.h"
#include "periph_handler.h"

//SPI通信ハンドラ
#define CONTROLLER_SPI_HANDLER		hspi3

//CSポート、ピン
#define CONTROLLER_CS_PORT			CS3_GPIO_Port
#define CONTROLLER_CS_PIN			CS3_Pin

//アナログスティック不感帯
#define ANALOG_STICK_DEADZONE		0.30f

//ボタンマスク用
enum {
	CONTROLLER_LEFT     = 0b1000000000000000,
	CONTROLLER_DOWN     = 0b0100000000000000,
	CONTROLLER_RIGHT    = 0b0010000000000000,
	CONTROLLER_UP       = 0b0001000000000000,
	CONTROLLER_START    = 0b0000100000000000,
	CONTROLLER_SELECT   = 0b0000000100000000,
	CONTROLLER_SQUARE   = 0b0000000010000000,
	CONTROLLER_CROSS    = 0b0000000001000000,
	CONTROLLER_CIRCLE   = 0b0000000000100000,
	CONTROLLER_TRIANGLE = 0b0000000000010000,
	CONTROLLER_L1       = 0b0000000000000100,
	CONTROLLER_L2       = 0b0000000000000001,
	CONTROLLER_R1       = 0b0000000000001000,
	CONTROLLER_R2       = 0b0000000000000010,
	CONTROLLER_LSTICK   = 0b0000001000000000,
	CONTROLLER_RSTICK   = 0b0000010000000000
};

//定数
enum {
	ANALOG_MODE = 0x01,
	DIGITAL_MODE = 0x00,
	MODECHANGE_EN = 0x00,
	MODECHANGE_DIS = 0x03,
};

typedef enum {
	VIB_SMALL_MOTOR_OFF = 0x00,
	VIB_SMALL_MOTOR_ON = 0x01
} vib_e;

//アナログスティック
typedef struct {
	int16_t x;
	int16_t y;
} analog_stick;

typedef struct {
	float x;
	float y;
	float r;
	float theta;
} analog_stick_f;

//プロトタイプ宣言
int8_t PSControllerInit(uint8_t, uint8_t);
void PSControllerGetData(uint16_t*, analog_stick_f*, analog_stick_f*, vib_e, uint8_t);
void PrintBin(uint8_t);


#endif /* CONTROLLER_H_ */

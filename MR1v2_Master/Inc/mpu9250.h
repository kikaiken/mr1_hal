/*
 * gyro.h
 *
 *  Created on: 2018/08/15
 *      Author: Ryohei
 */

#ifndef MPU9250_H_
#define MPU9250_H_

#include "stm32f7xx_hal.h"
#include "periph_handler.h"
#include "user_interface.h"
#include "robotstate.h"
#include <math.h>

//SPI通信ハンドラ
#define GYRO_SPI_HANDLER			hspi3

//CSポート、ピン
#define GYRO_CS_PORT				GPIOD
#define GYRO_CS_PIN					GPIO_PIN_2

//レジスタ
#define MPU9250_WHO_AM_I			0x75
#define MPU9250_SMPLRT_DIV			0x19
#define MPU9250_CONFIG				0x1A
#define MPU9250_GYRO_CONFIG			0x1B
#define MPU9250_ACCEL_CONFIG		0x1C
#define MPU9250_ACCEL_CONFIG_2		0x1D
#define MPU9250_INT_PIN_CFG			0x37
#define MPU9250_ACCEL_XOUT_H		0x3B
#define MPU9250_ACCEL_XOUT_L		0x3C
#define MPU9250_ACCEL_YOUT_H		0x3D
#define MPU9250_ACCEL_YOUT_L		0x3E
#define MPU9250_ACCEL_ZOUT_H		0x3F
#define MPU9250_ACCEL_ZOUT_L		0x40
#define MPU9250_GYRO_XOUT_H			0x43
#define MPU9250_GYRO_XOUT_L			0x44
#define MPU9250_GYRO_YOUT_H			0x45
#define MPU9250_GYRO_YOUT_L			0x46
#define MPU9250_GYRO_ZOUT_H			0x47
#define MPU9250_GYRO_ZOUT_L			0x48
#define MPU9250_PWR_MGMT_1      	0x6B
#define MPU9250_PWR_MGMT_2      	0x6C

//プロトタイプ宣言
void MPU9250GetAngVel(struct_state *state);
void MPU9250GetAngVelOffset(void);
void MPU9250Init(void);


#endif /* MPU9250_H_ */

/*
 * periph_handler.h
 *
 *  Created on: 2018/11/21
 *      Author: Ryohei
 */

#ifndef PERIPH_HANDLER_H_
#define PERIPH_HANDLER_H_

#include "main.h"

extern I2C_HandleTypeDef hi2c2;

extern SPI_HandleTypeDef hspi3;

extern TIM_HandleTypeDef htim1;
extern TIM_HandleTypeDef htim2;
extern TIM_HandleTypeDef htim3;
extern TIM_HandleTypeDef htim4;
extern TIM_HandleTypeDef htim5;
extern TIM_HandleTypeDef htim6;
extern TIM_HandleTypeDef htim7;
extern TIM_HandleTypeDef htim8;
extern TIM_HandleTypeDef htim14;

#endif /* PERIPH_HANDLER_H_ */

/*
 * robotstate.h
 *
 *  Created on: 2018/06/19
 *      Author: Ryohei
 */

#ifndef ROBOTSTATE_H_
#define ROBOTSTATE_H_


#include "stm32f7xx_hal.h"

//制御周期
#define CTRL_CYCLE		0.01f

typedef struct {
	float x;
	float y;
	float theta;
	float omega;
} struct_pos;


typedef struct {
	float vx;
	float vy;
	float vabs;
} struct_vel;

//ロボットの状態
typedef struct {
	volatile struct_vel prev_vel;
	volatile struct_vel vel;
	volatile struct_vel vel_diff_integ;

	volatile float angvel;

	volatile struct_pos prev_odometry;
	volatile struct_pos odometry;
	volatile struct_pos enc_odom;
	volatile struct_pos lidar_odom;
	volatile struct_pos odometry_diff_integ;

	volatile struct_pos location;
	volatile struct_pos diff_lidar;

	volatile struct_pos apply;

} struct_state;

//プロトタイプ宣言
void ClearRobotState(struct_state *state);

#endif /* ROBOTSTATE_H_ */

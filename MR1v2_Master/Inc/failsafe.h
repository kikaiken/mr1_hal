/*
 * failsafe.h
 *
 *  Created on: 2019/02/27
 *      Author: nabeya11
 */

#ifndef FAILSAFE_H_
#define FAILSAFE_H_

typedef enum{
	NO_ERROR			= 0x00,
	FINISH_PROCESS		= 0b00000001,
	CANCEL_PROCESS		= 0b00000010,

	TOO_DIFF_FROM_REF	= 0b00000100,
	OVER_SPEED			= 0b00001000,

	NO_MODE				= 0b00010000,
	FAIL_CONNECTION		= 0b00100000,

	UNKOWN_ERROR		= 0b10000000,
}failsafe_t;


#endif /* FAILSAFE_H_ */

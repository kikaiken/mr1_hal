/*
 * motor.h
 *
 *  Created on: 2018/11/25
 *      Author: nabeya11
 */

#ifndef MOTOR_H_
#define MOTOR_H_

#include "stm32f7xx_hal.h"
#include "periph_handler.h"
#include "robotstate.h"
#include "failsafe.h"

//速度制限
#define MAX_SPEED 1.00f
#define MAX_ROTATE 1.00f
#define MAX_SPEED_INDI 0.85f
#define MAX_DIFF_XY_REF 1.5f
#define MAX_DIFF_THETA_REF 30

//PID gain
#define V_PGAIN 0.55f
#define V_DGAIN 0.0f
#define V_IGAIN (0.0f * CTRL_CYCLE)

#define X_PGAIN 0.4f
#define X_DGAIN 0.0f
#define X_IGAIN 0.0f

#define T_PGAIN 0.6f
#define T_DGAIN 0.02f

#define O_PGAIN 0.08f
//オムニ割り当て
typedef enum undermotor{
	UNDERMOTOR_FL = TIM_CHANNEL_3,
	UNDERMOTOR_FR = TIM_CHANNEL_4,
	UNDERMOTOR_BL = TIM_CHANNEL_1,
	UNDERMOTOR_BR = TIM_CHANNEL_2
}undermotor_t;

void undermotor_init(void);
void undermotor_reset(void);
uint8_t UnderMotor_Raw(undermotor_t, float);
void UnderMotor_Stop(void);
uint8_t UnderMotor_xyot(float, float, float, float);
failsafe_t UnderMotor_Velocity_withPID(struct_state*, struct_pos*);
failsafe_t UnderMotor_Velocity_withOmega(struct_state*, struct_pos*);
failsafe_t UnderMotor_Location_withPID(struct_state*, struct_pos*);

void CheckOmniWheelDirection(void);

#endif /* MOTOR_H_ */

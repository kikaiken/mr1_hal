#ifndef ROSBRIDGE_PROXY_H
#define ROSBRIDGE_PROXY_H

#define F7_PORT 9087

#define PROXY_IP_ADDR1 192
#define PROXY_IP_ADDR2 168
#define PROXY_IP_ADDR3 0
#define PROXY_IP_ADDR4 88
#define PROXY_PORT 9088

#include "main.h"
#include "rosbridge_proxy.h"
#include "lwip.h"
#include "lwip/udp.h"

struct __attribute__((packed)) pkt_pose2d {
    uint8_t opcode;
    uint8_t padding[3];
    float x;
    float y;
    float theta;
};

struct __attribute__((packed)) vec3 {
    float x;
    float y;
    float z;
};

struct __attribute__((packed)) quaternion {
    float x;
    float y;
    float z;
    float w;
};

struct __attribute__((packed)) twist {
    struct vec3 linear;
    struct vec3 angular;
};

struct __attribute__((packed)) pose {
    struct vec3 point;
    struct quaternion orientation;
};

struct __attribute__((packed)) pkt_odom {
    uint8_t opcode;
    uint8_t padding[3];
    struct pose pose;
    struct twist twist;
};

struct __attribute__((packed)) transform {
    struct vec3 translation;
    struct quaternion rotation;
};


struct __attribute__((packed)) pkt_tf {
    uint8_t opcode;
    uint8_t padding[3];
    struct transform transform;
};
#define PROXY_DATA_POSE2D 1
#define PROXY_DATA_ODOM 2
#define PROXY_DATA_TF 3

void rosbridge_proxy_init();
void rosbridge_proxy_send();
void rosbridge_proxy_recv(void *arg, struct udp_pcb *pcb, struct pbuf *p, ip_addr_t *addr, u16_t port);
void rosbridge_proxy_send_odom(struct pkt_odom *odom);
void rosbridge_proxy_send_tf(struct pkt_tf *tf);
#endif

/*
 * mode.h
 *
 *  Created on: 2018/12/30
 *      Author: nabeya11
 */

#ifndef MODE_H_
#define MODE_H_


//モード種類
typedef enum{
	MODESW_MIN = 0,
	MODESW_0 = MODESW_MIN,
	MODESW_1,
	MODESW_2,
	MODESW_3,
	MODESW_4,
	MODESW_MAX = MODESW_4
}ModeSWState;

typedef enum{
	MODE_MT,
	MODE_LIDAR_SETUP,

	PHASE_FOREST,
	PHASE_GERGE,
	PHASE_SHAGAI1_PICKUP,
	PHASE_SHAGAI1_READY,
	PHASE_SHAGAI1_THROW,
	PHASE_SHAGAI2_PICKUP,
	PHASE_SHAGAI2_THROW,
	PHASE_SHAGAI3_PICKUP,
	PHASE_SHAGAI3_THROW,
	PHASE_SHAGAI_AUTOPICKUP,
	PHASE_TEST_PATH,

	PRE_PHASE_FOREST,
	PRE_PHASE_GERGE,
	PRE_PHASE_SHAGAI1_PICKUP,
	PRE_PHASE_SHAGAI1_READY,
	PRE_PHASE_SHAGAI1_THROW,
	PRE_PHASE_SHAGAI2_PICKUP,
	PRE_PHASE_SHAGAI2_THROW,
	PRE_PHASE_SHAGAI3_PICKUP,
	PRE_PHASE_SHAGAI3_THROW,
	PRE_PHASE_SHAGAI_AUTOPICKUP,
	PRE_PHASE_TEST_PATH,

	PRE_MODE_MT,
	PRE_MODE_LIDAR_SETUP,
}PhaseAndMode;

//モード移動用
typedef enum{
	MODE_MINUS,
	MODE_PLUS
}ModeDirTypeDef;


void Mode_UI(ModeSWState);
void Start_Buzzer(ModeSWState);
void Mode_Change(ModeSWState *mode,ModeDirTypeDef dir);
void Mode_Set(ModeSWState *mode,ModeSWState setmode);
void Phase_Shift(PhaseAndMode *phase,PhaseAndMode setphase);

#endif /* MODE_H_ */

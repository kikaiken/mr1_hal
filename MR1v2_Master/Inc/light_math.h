/*
 * light_math.h
 *
 *  Created on: 2019/02/27
 *      Author: nabeya11
 */

#ifndef LIGHT_MATH_H_
#define LIGHT_MATH_H_

#define RAD2DEG(x)	(float)((x) * 180 / M_PI)
#define DEG2RAD(x)	(float)((x) * M_PI / 180)

#define MAX(a, b)	(((a) > (b)) ? (a) : (b))
#define MIN(a, b)	(((a) < (b)) ? (a) : (b))

#endif /* LIGHT_MATH_H_ */

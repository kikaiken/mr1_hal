/*
 * trajectory.h
 *
 *  Created on: 2018/12/28
 *      Author: nabeya11
 */

#ifndef PATH_H_
#define PATH_H_


#include "stm32f7xx_hal.h"
#include "robotstate.h"
#include "user_interface.h"
#include "mode.h"

typedef enum {
	PF_FOLLOWING,
	PF_FINISH
} Path_StatusTypedef;

typedef enum {
	PATH_TYPE_THROW,
	PATH_TYPE_RETURN
} path_type_e;

//座標
//スローイングゾーンの入り口1個目
#define THROWING_ZONE_ENTRANCE_X_1	-2.85f
#define THROWING_ZONE_ENTRANCE_Y_1	8.50f
//スローイングゾーンの入り口23個目
#define THROWING_ZONE_ENTRANCE_X_23	-3.50f
#define THROWING_ZONE_ENTRANCE_Y_23	8.50f
//シャガイ投擲位置
#define SHAGAI_THROW_X				-3.80f
#define SHAGAI_THROW_Y				4.70f
//シャガイ1のピックアップ位置
#define SHAGAI1_PICKUP_X 			-4.50f
#define SHAGAI1_PICKUP_Y			8.75f
//シャガイ2のピックアップ位置
#define SHAGAI2_PICKUP_X 			-3.50f
#define SHAGAI2_PICKUP_Y			8.75f
//シャガイ3のピックアップ位置
#define SHAGAI3_PICKUP_X 			-2.50f
#define SHAGAI3_PICKUP_Y			8.75f

//経路配列サイズ
#define MAX_VELOCITY_MAP_LENGTH			1500
#define MAX_STRAIGHT_PATH_LENGTH		400
#define MAX_MERGED_PATH_LENGTH			1000
#define PATH_LONG_CM_START_TO_GERGE 	1353
#define PATH_LONG_CM_SHAGAI_THROW_1		417
#define PATH_LONG_CM_SHAGAI_THROW_23	385

//経路情報
extern float velocity_map[MAX_VELOCITY_MAP_LENGTH];

extern struct_pos start_to_gerge[PATH_LONG_CM_START_TO_GERGE];
extern struct_pos shagai_throw_1[PATH_LONG_CM_SHAGAI_THROW_1];
extern struct_pos shagai_throw_23[PATH_LONG_CM_SHAGAI_THROW_23];
extern struct_pos straight_path[MAX_STRAIGHT_PATH_LENGTH];
struct_pos merged_path[MAX_MERGED_PATH_LENGTH];

//プロトタイプ宣言
void PrepareNextPath(PhaseAndMode *, float, float, float, struct_state, ToggleState);
int32_t CalcPurePursuit(struct_state *, struct_pos *, Path_StatusTypedef *, ToggleState, PhaseAndMode);
void MakeVelocityMap(float, float, float, PhaseAndMode);


#endif /* PATH_H_ */

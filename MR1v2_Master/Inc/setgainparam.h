/*
 * setgainparam.h
 *
 *  Created on: 2018/06/19
 *      Author: Ryohei
 */

#ifndef SETGAINPARAM_H_
#define SETGAINPARAM_H_


#include "stm32f7xx_hal.h"
#include "robotstate.h"

//機体定数
typedef struct {
	float mass;
	float motor_r;
	float motor_km;
	float motor_ke;
	float wheel_radius;
	float gear_eff;
	uint8_t gear_ratio;
} struct_parameter;
extern struct_parameter parameter;

//プロトタイプ宣言
void SetParameter(void);


#endif /* SETGAINPARAM_H_ */

/*
 * dynamixel_com.h
 *
 *  Created on: 2019/03/26
 *      Author: naoki
 */

#ifndef CONTROLLER_DYNAMIXEL_COM_H_
#define CONTROLLER_DYNAMIXEL_COM_H_

#include <stdbool.h>
#include "main.h"
#include "controller.h"
#include "failsafe.h"
#define CONTROLLER_COM_START_0 0xFF
#define CONTROLLER_COM_START_1 0xFE

#define CONTROLLER_COM_END_0 0x00
#define CONTROLLER_COM_END_1 0x01

extern failsafe_t failsafe;

enum controller_com_recv_st {
    CONTROLLER_COM_RECV_ST_WAIT_START_0,
    CONTROLLER_COM_RECV_ST_WAIT_START_1,
    CONTROLLER_COM_RECV_ST_WAIT_DATA_0,
    CONTROLLER_COM_RECV_ST_WAIT_DATA_1,
    CONTROLLER_COM_RECV_ST_WAIT_DATA_2,
    CONTROLLER_COM_RECV_ST_WAIT_DATA_3,
    CONTROLLER_COM_RECV_ST_WAIT_DATA_4,
    CONTROLLER_COM_RECV_ST_WAIT_DATA_5,
    CONTROLLER_COM_RECV_ST_WAIT_END_0,
    CONTROLLER_COM_RECV_ST_WAIT_END_1,
};

void controller_com_init(UART_HandleTypeDef *huart);
void controller_com_proc();
void controller_com_get_data(uint16_t *button , analog_stick_f *left_stick, analog_stick_f * right_stick);
void controller_com_timer();
#endif

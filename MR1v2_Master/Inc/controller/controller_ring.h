/*
 * dynamixel_ring.h
 *
 *  Created on: 2019/03/26
 *      Author: naoki
 */

#ifndef CONTROLLER_CONTROLLER_RING_H_
#define CONTROLLER_CONTROLLER_RING_H_

#include "main.h"

#define RING_SUCCESS 0
#define RING_FAIL 1
#define CONTROLLER_RING_BUF_SIZE 512

struct controller_ring_buf {
  uint8_t buf[CONTROLLER_RING_BUF_SIZE];
  uint16_t buf_size;
  uint16_t w_ptr, r_ptr;
  uint16_t overwrite_cnt;
  UART_HandleTypeDef *huart;
};

void controller_ring_init(struct controller_ring_buf *ring, UART_HandleTypeDef *huart);
int controller_ring_getc(struct controller_ring_buf *ring, uint8_t *c);
int controller_ring_putc(struct controller_ring_buf *ring, uint8_t c);
int controller_ring_available(struct controller_ring_buf *ring);

#endif /* CONTROLLER_DYNAMIXEL_RING_H_ */

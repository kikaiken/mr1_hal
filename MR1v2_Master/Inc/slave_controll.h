/*
 * slave_controll.h
 *
 *  Created on: 2019/03/25
 *      Author: naoki
 */

#ifndef SLAVE_CONTROLL_H_
#define SLAVE_CONTROLL_H_

#include "main.h"

#define SLV_CMD_MIN 0
#define SLV_CMD_MAX 11

typedef enum {
	State_Reset				=  0,
	State_Forest			=  1,
	State_AfterBridge		=  2,
	State_AfterLine			=  3,
	State_AfterGerge		=  4,	//a
	State_ExtendPoker		=  5,	//b
	State_LockShagai		=  6,	//c
	State_ReadyToGrasp		=  7,	//d
	State_GraspAndElavate	=  8,	//e
	State_StorePoker		=  9,
	State_ReadyToShoot		= 10,	//f
	State_ShootShagai		= 11,

	State_Freeze			= 12,
	State_NULL				= 13,

	Adjust_RailAngle		= 20,
	Adjust_DynaAngle		= 21,

} AttitudeState;

void slave_com_send(uint8_t, uint32_t);

#endif /* SLAVE_CONTROLL_H_ */

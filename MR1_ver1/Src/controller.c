/*
 * controller.c
 *
 *  Created on: 2018/06/15
 *      Author: Ryohei
 */


#include "controller.h"
#include <stdlib.h>
#include <math.h>

//static関数プロトタイプ宣言
static float NormalizeStick(uint8_t, int16_t);
static void PSControllerSelect(void);
static void PSControllerDeselect(void);
static void PSControllerWait(void);
static void Norm2Polar(analog_stick_f*);

/*
 * プレステコントローラ初期化
 * @param mode アナログモードorデジタルモード
 * @param modechange modeボタンでモードチェンジ許可/不許可
 * @return	通信失敗は-1,成功で0
 */
int8_t PSControllerInit(uint8_t mode, uint8_t modechange) {
	uint8_t tx_data[4][10] = {		//http://store.curiousinventor.com/guides/PS2#hardware
			{0x01, 0x43, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00},//Enter configmode
			{0x01, 0x44, 0x00, mode, modechange, 0x00, 0x00, 0x00, 0x00},//set mode&lock
			{0x01, 0x4D, 0x00, 0x00, 0x01, 0xFF, 0xFF, 0xFF, 0xFF},//enable vibration motor
			{0x01, 0x43, 0x00, 0x00, 0x5A, 0x5A, 0x5A, 0x5A, 0x5A},//exit configmode
	};
	uint8_t rx_data[10] = {};

	//通信設定変更
	CONTROLLER_SPI_HANDLER.Init.FirstBit = SPI_FIRSTBIT_LSB;
	CONTROLLER_SPI_HANDLER.Init.CLKPhase = SPI_PHASE_2EDGE;
	HAL_SPI_Init(&CONTROLLER_SPI_HANDLER);

	//通信開始
	for(uint8_t j = 0; j < 4; j++){
		PSControllerSelect();
		HAL_Delay(1);
		for (uint8_t i = 0; i < 9; i++) {
			HAL_SPI_TransmitReceive(&CONTROLLER_SPI_HANDLER, &tx_data[j][i], &rx_data[i], 1, 0xFFFF);
			PSControllerWait();
		}
		HAL_Delay(1);
		PSControllerDeselect();
		HAL_Delay(10);
	}

	HAL_TIM_Base_Start_IT(&htim7);

	return 0;
}

/*
 * プレステコントローラ通信
 * @param	button : ボタンのデータを16進数で返す(各ビットが各ボタンに対応する)
 * 			left_stick : 左のアナログスティックの傾きを(x,y)表示(各軸に対して-1 ~ 1)
 * 			right_stick : 同上(右アナログスティック)
 * 			left_polar : 左のアナログスティックの傾きを(r,θ)表示(r:-1~1, θ:0~360)
 * 			right_polar : 同上(右アナログスティック)
 * 			small_motor : 小モータVIB_SMALL_MOTOR_ON / VIB_SMALL_MOTOR_OFF
 * 			large_motor : 大モータの出力(0 ~ 255)
 * @return
 */
void PSControllerGetData(uint16_t *button , analog_stick_f *left_stick, analog_stick_f * right_stick, vib_e small_motor, uint8_t large_motor) {
	analog_stick left_offset, right_offset;
	int8_t data_num;
	uint8_t tx_data[10] = {0x01, 0x42, 0x00, small_motor, large_motor};
	uint8_t rx_data[10] = {};

	//オフセット設定
	left_offset.x = 128;
	left_offset.y = 128;
	right_offset.x = 128;
	right_offset.y = 128;

	//通信設定変更
	CONTROLLER_SPI_HANDLER.Init.FirstBit = SPI_FIRSTBIT_LSB;
	CONTROLLER_SPI_HANDLER.Init.CLKPhase = SPI_PHASE_2EDGE;
	HAL_SPI_Init(&CONTROLLER_SPI_HANDLER);

	//通信開始
	PSControllerSelect();
	PSControllerWait();
	for (uint8_t i = 0; i < 3; i++) {
		HAL_SPI_TransmitReceive(&CONTROLLER_SPI_HANDLER, &tx_data[i], &rx_data[i], 1, 0xFFFF);
		PSControllerWait();
	}

	//アナログスティックを使用するかを確認
	if ((rx_data[1] & 0x0F) == 0x03) {data_num = 3 + 6;}	//使用する場合
	else 							 {data_num = 3 + 2;}	//使用しない場合

	//アナログスティック読み込み
	for (int8_t i = 3; i < data_num; i++) {
		HAL_SPI_TransmitReceive(&CONTROLLER_SPI_HANDLER, &tx_data[i], &rx_data[i], 1, 0xFFFF);
		PSControllerWait();
	}
	PSControllerDeselect();

	//ボタンの処理
	//全ビット反転、16ビットに結合して返す
	rx_data[3] = ~rx_data[3];
	rx_data[4] = ~rx_data[4];
	*button = ((rx_data[3] << 8) + rx_data[4]);

	//アナログスティックの処理
	//スティックの値を-1~1に正規化
	if ((rx_data[1] & 0x0F) == 0x03) {		//アナログスティックを使用する場合
		//(x,y)を-1~1に正規化したもの
		left_stick->x  = NormalizeStick(rx_data[7], left_offset.x);
		left_stick->y  = NormalizeStick(rx_data[8], left_offset.y)  * (-1);
		right_stick->x = NormalizeStick(rx_data[5], right_offset.x);
		right_stick->y = NormalizeStick(rx_data[6], right_offset.y) * (-1);

	} else {		//アナログスティックを使用しない場合は全てゼロ
		left_stick->x  = 0;
		left_stick->y  = 0;
		right_stick->x = 0;
		right_stick->y = 0;
	}

	//極座標計算&deadzone
	Norm2Polar(left_stick);
	Norm2Polar(right_stick);
}

/*
 * スティックの値を直交座標から極座標へ
 * @param	stick_norm	直交座標(x,y)表示(各軸に対して-1 ~ 1)
 * @param	stick	極座標(r,θ)表示(r:-1~1, θ:0~360)
 * @return
 */
static void Norm2Polar(analog_stick_f *stick) {
	//calc r
	stick->r = fminf(hypotf(stick->x, stick->y), 1.0f);

	//deadzone
	if (stick->r < ANALOG_STICK_DEADZONE) {
		stick->r = 0;
		stick->theta = 0;
		stick->x = stick->y = 0;
	}

	//calc theta
	if (stick->r != 0) {
		stick->theta = atan2f(stick->x,stick->y);
//		stick->theta = (180 * asinf(stick->y / stick->r)) / M_PI;
//		if (stick->x > 0 && stick->y > 0) {			//第一象限
//			;
//		} else if (stick->x < 0 && stick->y > 0) {	//第二象限
//			stick->theta = 180 - stick->theta;
//		} else if (stick->x < 0 && stick->y < 0) {	//第三象限
//			stick->theta = 180 - stick->theta;
//		} else {											//第四象限
//			stick->theta = 360 + stick->theta;
//		}
	} else {
		stick->theta = 0;
	}
}

/*
 * スティックの値を-1 ~ 1に正規化
 * @param
 * @return
 */
static float NormalizeStick(uint8_t raw_stick_data, int16_t offset) {
	return ((float)(raw_stick_data - offset) / (float)offset);
}

/*
 * プレステコントローラチップセレクト
 * @param
 * @return
 */
static void PSControllerSelect(void) {
	HAL_GPIO_WritePin(CONTROLLER_CS_PORT, CONTROLLER_CS_PIN, GPIO_PIN_RESET);
}

/*
 * プレステコントローラチップセレクト解除
 * @param
 * @return
 */
static void PSControllerDeselect(void) {
	HAL_GPIO_WritePin(CONTROLLER_CS_PORT, CONTROLLER_CS_PIN, GPIO_PIN_SET);
}

/*
 * プレステコントローラ送受信間の待ち時間
 * @param
 * @return
 * @note	テキトー。大体で動く。
 */
static void PSControllerWait(void) {
	volatile uint32_t i = 300;
	while (i--) ;
}

/*
 * 2進数表示
 * @param	x : 8ビットの数値
 * @return
 */
void PrintBin(uint8_t x) {
	uint8_t mask = 0b10000000;
	for (int i = 0; i < 8; i++) {
		if (x & (mask >> i)) printf("%d ", 1);
		else 				 printf("%d ", 0);
	}
}

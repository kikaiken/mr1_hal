/*
 * motor.c
 *
 *  Created on: 2018/11/25
 *      Author: nabeya11
 */

#include <math.h>
#include <stdlib.h>
#include "main.h"
#include "motor.h"


/*-----------------------------------------------
 * OmniWheel
 ----------------------------------------------*/
/*
 * オムニ初期化
 * @param
 * @return
 */
void undermotor_init(void){
	HAL_GPIO_WritePin(RST1_GPIO_Port,RST1_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(RST2_GPIO_Port,RST2_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(RST3_GPIO_Port,RST3_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(RST4_GPIO_Port,RST4_Pin,GPIO_PIN_SET);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_4);
}
/*
 * オムニリセット
 * @param
 * @return
 */
void undermotor_reset(void){
	HAL_GPIO_WritePin(RST1_GPIO_Port,RST1_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(RST2_GPIO_Port,RST2_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(RST3_GPIO_Port,RST3_Pin,GPIO_PIN_RESET);
	HAL_GPIO_WritePin(RST4_GPIO_Port,RST4_Pin,GPIO_PIN_RESET);
	HAL_Delay(500);
	HAL_GPIO_WritePin(RST1_GPIO_Port,RST1_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(RST2_GPIO_Port,RST2_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(RST3_GPIO_Port,RST3_Pin,GPIO_PIN_SET);
	HAL_GPIO_WritePin(RST4_GPIO_Port,RST4_Pin,GPIO_PIN_SET);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim1, TIM_CHANNEL_4);
}
/*
 * 個々のオムニ駆動
 * @param	undermotor_t mchannel : motor.h内に指定
 * *		float mspeed : 速度　-1~1
 * @return	uint8_t : 速度オーバーなら-1
 */
uint8_t undermotor_raw(undermotor_t mchannel, float mspeed){
	if(abs(mspeed) <= MAX_SPEED_INDI){
		switch (mchannel) {
			case TIM_CHANNEL_1:
				if(mspeed>0) HAL_GPIO_WritePin(PHASE1_GPIO_Port,PHASE1_Pin,GPIO_PIN_SET);
				else HAL_GPIO_WritePin(PHASE1_GPIO_Port,PHASE1_Pin,GPIO_PIN_RESET);
				break;
			case TIM_CHANNEL_2:
				if(mspeed>0) HAL_GPIO_WritePin(PHASE2_GPIO_Port,PHASE2_Pin,GPIO_PIN_SET);
				else HAL_GPIO_WritePin(PHASE2_GPIO_Port,PHASE2_Pin,GPIO_PIN_RESET);
				break;
			case TIM_CHANNEL_3:
				if(mspeed>0) HAL_GPIO_WritePin(PHASE3_GPIO_Port,PHASE3_Pin,GPIO_PIN_SET);
				else HAL_GPIO_WritePin(PHASE3_GPIO_Port,PHASE3_Pin,GPIO_PIN_RESET);
				break;
			case TIM_CHANNEL_4:
				if(mspeed>0) HAL_GPIO_WritePin(PHASE4_GPIO_Port,PHASE4_Pin,GPIO_PIN_SET);
				else HAL_GPIO_WritePin(PHASE4_GPIO_Port,PHASE4_Pin,GPIO_PIN_RESET);
				break;
			default:
				mspeed=0;
				break;
		}

		uint32_t mspeed_32=fabsf(mspeed*__HAL_TIM_GET_AUTORELOAD(&htim1));
		__HAL_TIM_SET_COMPARE(&htim1, mchannel, mspeed_32);
		return 0;
	}
	else{
		return 1;
	}

}

/*
 * オムニx-y-omega駆動
 * @param	float x_speed : 正は右方向
 * 			float y_speed : 正は前方向
 * 			float m_omega : 正は左回転
 * @return
 */
void undermotor_xy(float x_speed, float y_speed, float m_omega){
#if 0
	float abs_x, abs_y;
	float real_x, real_y;

	if (x_speed == 0 && y_speed == 0) {
		real_x = real_y = 0;
	} else {
		abs_x=fabsf(x_speed);
		abs_y=fabsf(y_speed);
		real_x=x_speed*(abs_x>abs_y?abs_x:abs_y)/(abs_x+abs_y);
		real_y=y_speed*(abs_x>abs_y?abs_x:abs_y)/(abs_x+abs_y);
	}

	undermotor_raw(UNDERMOTOR_FL, -real_x - real_y + m_omega);
	undermotor_raw(UNDERMOTOR_FR, -real_x + real_y + m_omega);
	undermotor_raw(UNDERMOTOR_BL,  real_x - real_y + m_omega);
	undermotor_raw(UNDERMOTOR_BR,  real_x + real_y + m_omega);
#else
	undermotor_raw(UNDERMOTOR_FL, -x_speed - y_speed + m_omega);
	undermotor_raw(UNDERMOTOR_FR, -x_speed + y_speed + m_omega);
	undermotor_raw(UNDERMOTOR_BL,  x_speed - y_speed + m_omega);
	undermotor_raw(UNDERMOTOR_BR,  x_speed + y_speed + m_omega);
#endif
}

/*
 * オムニx-y-omega駆動
 * @param	float x_speed : 正は右方向
 * 			float y_speed : 正は前方向
 * 			float m_omega : 正は左回転
 * 			float m_theta : 正は左回転
 * @return 速度超過で-1,他で0
 */
uint8_t undermotor_xy_next(float x_speed, float y_speed, float m_omega, float m_theta){
	uint8_t mparam_over=0;

	if(x_speed>MAX_SPEED || y_speed>MAX_SPEED || m_omega>MAX_ROTATE){
		mparam_over=-1;
	}
	else{
		mparam_over+=undermotor_raw(UNDERMOTOR_FL, -x_speed*cosf(m_theta+M_PI_4) - y_speed*sinf(m_theta+M_PI_4) + m_omega);
		mparam_over+=undermotor_raw(UNDERMOTOR_FR, -x_speed*sinf(m_theta+M_PI_4) + y_speed*cosf(m_theta+M_PI_4) + m_omega);
		mparam_over+=undermotor_raw(UNDERMOTOR_BL,  x_speed*sinf(m_theta+M_PI_4) - y_speed*cosf(m_theta+M_PI_4) + m_omega);
		mparam_over+=undermotor_raw(UNDERMOTOR_BR,  x_speed*cosf(m_theta+M_PI_4) + y_speed*sinf(m_theta+M_PI_4) + m_omega);
	}

	if(mparam_over){
		undermotor_xy(0,0,0);
	}

	return mparam_over;
}


/*-----------------------------------------------
 * OtherMotor
 ----------------------------------------------*/
/*
 * 他のモータ初期化
 * @param
 * @return
 */
void Generalmotor_init(void){
	HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_1);
	HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_2);
	HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_3);
	HAL_TIM_PWM_Start(&htim8, TIM_CHANNEL_4);
}

/*
 * 他のモータ駆動
 * @param	uint32_t g_channnel 駆動チャンネル
 * 			float mspeed 速度-1~1
 * @return
 */
void Generalmotor(uint32_t g_channel,float mspeed){
	switch (g_channel) {
		case TIM_CHANNEL_1:
			if(mspeed == 0){
				HAL_GPIO_WritePin(INA5_GPIO_Port,INA5_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(INB5_GPIO_Port,INB5_Pin,GPIO_PIN_RESET);
			}
			else if(mspeed > 0){
				HAL_GPIO_WritePin(INA5_GPIO_Port,INA5_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(INB5_GPIO_Port,INB5_Pin,GPIO_PIN_RESET);
			}
			else{
				HAL_GPIO_WritePin(INA5_GPIO_Port,INA5_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(INB5_GPIO_Port,INB5_Pin,GPIO_PIN_SET);
			}
			break;
		case TIM_CHANNEL_2:
			if(mspeed == 0){
				HAL_GPIO_WritePin(INA6_GPIO_Port,INA6_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(INB6_GPIO_Port,INB6_Pin,GPIO_PIN_RESET);
			}
			else if(mspeed > 0){
				HAL_GPIO_WritePin(INA6_GPIO_Port,INA6_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(INB6_GPIO_Port,INB6_Pin,GPIO_PIN_RESET);
			}
			else{
				HAL_GPIO_WritePin(INA6_GPIO_Port,INA6_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(INB6_GPIO_Port,INB6_Pin,GPIO_PIN_SET);
			}
			break;
		case TIM_CHANNEL_3:
			if(mspeed == 0){
				HAL_GPIO_WritePin(INA7_GPIO_Port,INA7_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(INB7_GPIO_Port,INB7_Pin,GPIO_PIN_RESET);
			}
			else if(mspeed > 0){
				HAL_GPIO_WritePin(INA7_GPIO_Port,INA7_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(INB7_GPIO_Port,INB7_Pin,GPIO_PIN_RESET);
			}
			else{
				HAL_GPIO_WritePin(INA7_GPIO_Port,INA7_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(INB7_GPIO_Port,INB7_Pin,GPIO_PIN_SET);
			}
			break;
		case TIM_CHANNEL_4:
			if(mspeed == 0){
				HAL_GPIO_WritePin(INA8_GPIO_Port,INA8_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(INB8_GPIO_Port,INB8_Pin,GPIO_PIN_RESET);
			}
			else if(mspeed > 0){
				HAL_GPIO_WritePin(INA8_GPIO_Port,INA8_Pin,GPIO_PIN_SET);
				HAL_GPIO_WritePin(INB8_GPIO_Port,INB8_Pin,GPIO_PIN_RESET);
			}
			else{
				HAL_GPIO_WritePin(INA8_GPIO_Port,INA8_Pin,GPIO_PIN_RESET);
				HAL_GPIO_WritePin(INB8_GPIO_Port,INB8_Pin,GPIO_PIN_SET);
			}
			break;
		default:
			mspeed=0;
			return;
	}

	uint32_t mspeed_32=fabsf(mspeed*__HAL_TIM_GET_AUTORELOAD(&htim8));
	__HAL_TIM_SET_COMPARE(&htim8, g_channel, mspeed_32);
}


/*-----------------------------------------------
 * Servo
 ----------------------------------------------*/
/*
 * サーボモータ初期化
 * @param
 * @return
 */
void Servomotor_init(void){
	HAL_TIM_PWM_Start(&SERVO_1, TIM_CHANNEL_1);
//	HAL_TIM_PWM_Start(&SERVO_2, TIM_CHANNEL_1);
//	HAL_TIM_PWM_Start(&SERVO_3, TIM_CHANNEL_1);
}

/*
 * サーボモータ駆動
 * @param	TIM_HandleTypeDef *htims 駆動タイマ
 * 			float value 角度0~180
 * @return
 */
void Servomotor(TIM_HandleTypeDef *htims,uint32_t value){
	__HAL_TIM_SET_COMPARE(htims, TIM_CHANNEL_1, value * 4 + 1800);
}


/*-----------------------------------------------
 * Valve
 ----------------------------------------------*/
/*
 * 電磁弁オンオフ
 * @param	valve_t valve_num 駆動電磁弁
 * 			valvestate_t valve_state ON/OFF
 * @return
 */
void Valve(valve_t valve_num, valvestate_t valve_state){
	switch(valve_num){
		case VALVE1:
			HAL_GPIO_WritePin(valve1_GPIO_Port,valve1_Pin,valve_state);
			break;
		case VALVE2:
			HAL_GPIO_WritePin(valve2_GPIO_Port,valve2_Pin,valve_state);
			break;
		case VALVE3:
			HAL_GPIO_WritePin(valve3_GPIO_Port,valve3_Pin,valve_state);
			break;
		default:
			break;
	}
}

/*
 * 電磁弁トグル
 * @param	valve_t valve_num 駆動電磁弁
 * @return
 */
void Valve_Toggle(valve_t valve_num){
	switch(valve_num){
		case VALVE1:
			HAL_GPIO_TogglePin(valve1_GPIO_Port,valve1_Pin);
			break;
		case VALVE2:
			HAL_GPIO_TogglePin(valve2_GPIO_Port,valve2_Pin);
			break;
		case VALVE3:
			HAL_GPIO_TogglePin(valve3_GPIO_Port,valve3_Pin);
			break;
		default:
			break;
	}
}

/*
 * robotstate.h
 *
 *  Created on: 2018/06/19
 *      Author: Ryohei
 */

#ifndef ROBOTSTATE_H_
#define ROBOTSTATE_H_


#include "stm32f7xx_hal.h"

//制御周期
#define CTRL_CYCLE		0.001f

typedef struct {
	float x;
	float y;
	float theta;
} struct_pos;

//ロボットの状態
typedef struct {
	volatile float prev_vel;
	volatile float vel;
	volatile float vel_err_integ;
	volatile float angvel;
	volatile float angvel_err_integ;
	volatile struct_pos odometry;
	volatile float battery_v;
} struct_state;

//プロトタイプ宣言
void ClearRobotState(struct_state *state);

#endif /* ROBOTSTATE_H_ */

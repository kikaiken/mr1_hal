/*
 * encoder.cpp
 *
 *  Created on: 2018/05/23
 *      Author: Ryohei
 */


#include "encoder.h"

//static関数プロトタイプ宣言
static float EncoderCount2Meter(int32_t);

/*
 * エンコーダ値からロボットの速さを求める。オドメトリも求める。
 * @param
 * @return
 * @note	CTRL_CYCLE(s)毎に呼び出すこと。
 */
void EncoderGetData(struct_state *state) {
	static int32_t enc_x_prev_cnt = ENC_CNT_RESET, enc_y_prev_cnt = ENC_CNT_RESET;
	volatile float dif_x, dif_y;

#ifdef ENC_X_FW_CNTUP
	dif_x = EncoderCount2Meter(TIMx_ENC_X->CNT - enc_x_prev_cnt);
#endif
#ifdef ENC_X_FW_CNTDOWN
	dif_x = EncoderCount2Meter(enc_x_prev_cnt - TIMx_ENC_X->CNT);
#endif

#ifdef ENC_Y_FW_CNTUP
	dif_y = EncoderCount2Meter(TIMx_ENC_Y->CNT - enc_y_prev_cnt);
#endif
#ifdef ENC_Y_FW_CNTDOWN
	dif_y = EncoderCount2Meter(enc_y_prev_cnt - TIMx_ENC_Y->CNT);
#endif

	//現在の速さ

	//オドメトリの計算
	state->odometry.x += dif_x * cosf(state->odometry.theta) - dif_y * sinf(state->odometry.theta);
	state->odometry.y += dif_x * sinf(state->odometry.theta) + dif_y * cosf(state->odometry.theta);

	//カウント値リセット
	if ((TIMx_ENC_X->CNT > 60035) || (TIMx_ENC_X->CNT < 5000)) {	//X方向エンコーダカウント値がオーバーフローしそうになったらリセット
		TIMx_ENC_X->CNT = ENC_CNT_RESET;
	}
	if ((TIMx_ENC_Y->CNT > 60035) || (TIMx_ENC_Y->CNT < 5000)) {	//Y方向エンコーダカウント値がオーバーフローしそうになったらリセット
		TIMx_ENC_Y->CNT = ENC_CNT_RESET;
	}
	//カウント値保存
	enc_x_prev_cnt = TIMx_ENC_X->CNT;
	enc_y_prev_cnt = TIMx_ENC_Y->CNT;
}

/*
 * エンコーダカウント値をメートルに変換
 * @param	enc_cnt : エンコーダのカウント値
 * @return	長さ(m)
 */
static float EncoderCount2Meter(int32_t enc_cnt) {
	return (float)((float)(enc_cnt * M_PI * (float)ENC_OMNI_RADIUS) / (float)ENC_RESOLUTION);
}

/*
 * エンコーダカウントスタート
 * @param
 * @return
 */
void EncoderEnable(void) {
	HAL_TIM_Encoder_Start(&ENC_X_TIM_HANDLER, TIM_CHANNEL_ALL);
	HAL_TIM_Encoder_Start(&ENC_Y_TIM_HANDLER, TIM_CHANNEL_ALL);
	TIMx_ENC_X->CNT = ENC_CNT_RESET;
	TIMx_ENC_Y->CNT = ENC_CNT_RESET;
}

/*
 * エンコーダカウントストップ
 * @param
 * @return
 */
void EncoderDisable(void) {
	HAL_TIM_Encoder_Stop(&ENC_X_TIM_HANDLER, TIM_CHANNEL_ALL);
	HAL_TIM_Encoder_Stop(&ENC_Y_TIM_HANDLER, TIM_CHANNEL_ALL);
	TIMx_ENC_X->CNT = ENC_CNT_RESET;
	TIMx_ENC_Y->CNT = ENC_CNT_RESET;
}


/*
 * start AngEncorder
 * @param
 * @return
 */
void AngleEncoderEnable(void) {
	__HAL_TIM_SET_COUNTER(&ENC_ANG_HANDLER, 5000);
	HAL_TIM_Encoder_Start(&ENC_ANG_HANDLER, TIM_CHANNEL_ALL);
}

/*
 * エンコーダ値からランチャーの角度を検出
 * @param
 * @return
 * @note	CTRL_CYCLE(s)毎に呼び出すこと。
 */
void AngleEncoderGet(int32_t *angpos) {

	//calc position
#ifdef ENC_ANG_FW_CNTUP
	*angpos = __HAL_TIM_GET_COUNTER(&ENC_ANG_HANDLER);
#endif
#ifdef ENC_ANG_FW_CNTDOWN
	*angpos = -__HAL_TIM_GET_COUNTER(&ENC_ANG_HANDLER);
#endif

}

/*
 * gyro.c
 *
 *  Created on: 2018/08/15
 *      Author: Ryohei
 */

#include "mpu9250.h"

//マクロ
#define DEG2RAD(x)		(float)((x) * M_PI / 180)
#define RAD2DEG(x)		(float)((x) * 180 / M_PI)

//ファイル内グローバル変数
volatile static float angvel_offset = 0;

//static関数プロトタイプ宣言
static int8_t MPU9250InitSub(void);
static void MPU9250Send(uint8_t, uint8_t);
static uint8_t MPU9250Read(uint8_t);
static void MPU9250Select(void);
static void MPU9250Deselect(void);

/*
 * ジャイロZ軸入力
 * @param
 * @return	角速度(rad/s)
 */
void MPU9250GetAngVel(struct_state *state) {
	volatile uint8_t data_h, data_l;
	volatile int16_t data;

	//通信設定変更
	GYRO_SPI_HANDLER.Init.FirstBit = SPI_FIRSTBIT_MSB;
	GYRO_SPI_HANDLER.Init.CLKPhase = SPI_PHASE_2EDGE;
	HAL_SPI_Init(&GYRO_SPI_HANDLER);

	data_h = MPU9250Read(MPU9250_GYRO_ZOUT_H);
	data_l = MPU9250Read(MPU9250_GYRO_ZOUT_L);
	data = (data_h << 8) + data_l;

	if (data >= 0) state->angvel = (DEG2RAD(data * 0.06103516f) - angvel_offset) * 1.0;
	else           state->angvel = (DEG2RAD(data * 0.06103516f) - angvel_offset) * 1.0;
	state->odometry.theta += state->angvel * CTRL_CYCLE;
//	if (state->odometry.theta > M_PI) {
//		state->odometry.theta -= 2 * M_PI;
//	} else if (state->odometry.theta < -M_PI) {
//		state->odometry.theta += 2 * M_PI;
//	}
}

/*
 * 角速度のゼロ点を測定
 * @param
 * @return
 */
void MPU9250GetAngVelOffset(void) {
	struct_state state;
	ClearRobotState(&state);
	volatile float angvel_sum = 0;
	const uint16_t num_of_sample = 500;
	angvel_offset = 0;

	HAL_Delay(500);

	//num_of_sample回の測定の平均をとる
	for (int32_t i = 0; i < num_of_sample; i++) {
		MPU9250GetAngVel(&state);
		angvel_sum += state.angvel;
		HAL_Delay(1);
	}
	angvel_offset = angvel_sum / num_of_sample;

	//表示
	printf("MPU9250 Angular Velocity Offset = %f\n", angvel_offset);
}

/*
 * 初期設定複数回呼び出し
 * @param
 * @return
 * @note	電源投入直後は不安定になる可能性があるため
 */
void MPU9250Init(void) {
	volatile int8_t check;
	volatile int8_t count = 0;
	HAL_Delay(10);
	do{
		check = MPU9250InitSub();
		count++;
		if (count >= 10) {
			printf("MPU9250 Communication Error\n");
			return;
		}
	} while (check);
	printf("MPU9250 Communication Succeeded\n");
}

/*
 * 初期設定
 * @param
 * @return
 */
static int8_t MPU9250InitSub(void) {
	//通信設定変更
	GYRO_SPI_HANDLER.Init.FirstBit = SPI_FIRSTBIT_MSB;
	HAL_SPI_Init(&GYRO_SPI_HANDLER);

	//WHO_AM_I
	uint8_t gyro_check;
	gyro_check = MPU9250Read(MPU9250_WHO_AM_I);
	printf("MPU9250 Who am I = %d\n", gyro_check);

	//設定
	MPU9250Send(MPU9250_PWR_MGMT_1,  0x00);
	MPU9250Send(MPU9250_PWR_MGMT_2,  0b111110);		//ジャイロZ軸のみENABLE
	MPU9250Send(MPU9250_INT_PIN_CFG, 0x02);
	MPU9250Send(MPU9250_GYRO_CONFIG, 0x18);    		//Full Scale : 2000dps

	if (gyro_check != 0x71){
		return 1;
	} else {
		return 0;
	}
}

/*
 * データ書き込み
 * @param	address : MPU9250レジスタアドレス
 * 			data : 書き込むデータ
 * @return
 */
static void MPU9250Send(uint8_t address, uint8_t data) {
	uint8_t txdata[2] = {address, data};
	uint8_t rxdata[2] = {};
	MPU9250Select();
	HAL_SPI_TransmitReceive(&GYRO_SPI_HANDLER, &txdata[0], &rxdata[0], 1, HAL_MAX_DELAY);
	HAL_SPI_TransmitReceive(&GYRO_SPI_HANDLER, &txdata[1], &rxdata[1], 1, HAL_MAX_DELAY);
	MPU9250Deselect();
}

/*
 * データ読み込み
 * @param	address : MPU9250レジスタアドレス
 * @return	読み込んだデータ
 */
static uint8_t MPU9250Read(uint8_t address) {
	uint8_t txdata[2] = {(address | 0x80), 0x00};
	uint8_t rxdata[2] = {};
	MPU9250Select();
	HAL_SPI_TransmitReceive(&GYRO_SPI_HANDLER, &txdata[0], &rxdata[0], 1, HAL_MAX_DELAY);
	HAL_SPI_TransmitReceive(&GYRO_SPI_HANDLER, &txdata[1], &rxdata[1], 1, HAL_MAX_DELAY);
	MPU9250Deselect();
	return rxdata[1];
}

/*
 * チップセレクト
 * @param
 * @return
 */
static void MPU9250Select(void) {
	HAL_GPIO_WritePin(GYRO_CS_PORT, GYRO_CS_PIN, GPIO_PIN_RESET);
}

/*
 * チップセレクト解除
 * @param
 * @return
 */
static void MPU9250Deselect(void) {
	HAL_GPIO_WritePin(GYRO_CS_PORT, GYRO_CS_PIN, GPIO_PIN_SET);
}

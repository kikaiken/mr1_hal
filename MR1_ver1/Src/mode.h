/*
 * mode.h
 *
 *  Created on: 2018/12/30
 *      Author: nabeya11
 */

#ifndef MODE_H_
#define MODE_H_

#include "user_interface.h"

//#define MODE_MAX 3
//#define MODE_MIN 0

//モード種類
typedef enum{
	MODE_MIN = 0,
	MODE_0 = MODE_MIN,
	MODE_1,
	MODE_2,
	MODE_3,
	MODE_MAX = MODE_3
}startmode_t;

//モード移動用
typedef enum{
	MODE_MINUS,
	MODE_PLUS
}modedir_t;


void Mode_Change(startmode_t *mode,modedir_t dir);
void Mode_Set(startmode_t *mode,startmode_t setmode);

#endif /* MODE_H_ */

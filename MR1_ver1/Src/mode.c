/*
 * mode.c
 *
 *  Created on: 2018/12/30
 *      Author: nabeya11
 */

#include "mode.h"


/*
 * モードに対するブザー
 * @param *mode	モード変数のポインタ
 * @return
 */
void Mode_Buzzer(startmode_t *mode){
	switch(*mode){
		case MODE_0:	BuzzerBeep(BUZZER_E6,100);	break;
		case MODE_1:	BuzzerBeep(BUZZER_F6,100);	break;
		case MODE_2:	BuzzerBeep(BUZZER_G6,100);	break;
		case MODE_3:	BuzzerBeep(BUZZER_A6,100);	break;
		default:		UIWarning();				break;
	}
}

/*
 * モード変更
 * @param *mode	モード変数のポインタ
 * @param  dir	モード加減算
 * @return
 */
void Mode_Change(startmode_t *mode,modedir_t dir){
	if(dir == MODE_PLUS){
		*mode == MODE_MAX ? *mode = MODE_MIN : (*mode)++;
	}
	else if(dir == MODE_MINUS){
		*mode == MODE_MIN ? *mode = MODE_MAX : (*mode)--;
	}
	else{
		*mode = MODE_MIN;
	}
	Mode_Buzzer(mode);
}

/*
 * モード変更
 * @param *mode	モード変数のポインタ
 * @param  setmode	設定するモード
 * @return
 */
void Mode_Set(startmode_t *mode,startmode_t setmode){
	*mode = setmode;
	Mode_Buzzer(mode);
}

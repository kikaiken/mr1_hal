/*
 * motor.h
 *
 *  Created on: 2018/11/25
 *      Author: nabeya11
 */

#ifndef MOTOR_H_
#define MOTOR_H_

#include "stm32f7xx_hal.h"
#include "periph_handler.h"

//速度制限
#define MAX_SPEED 0.30f
#define MAX_ROTATE 0.30f
#define MAX_SPEED_INDI 0.50f

//オムニ割り当て
typedef enum undermotor{
	UNDERMOTOR_FL = TIM_CHANNEL_3,
	UNDERMOTOR_FR = TIM_CHANNEL_2,
	UNDERMOTOR_BL = TIM_CHANNEL_4,
	UNDERMOTOR_BR = TIM_CHANNEL_1
}undermotor_t;

//バルブ割り当て
#define VALVE_HAND VALVE2
#define VALVE_LAUNCH VALVE1

//バルブ状態割り当て
#define HAND_FREE VALVE_ON
#define HAND_PICK VALVE_OFF
#define LAUNCH_WAIT VALVE_ON
#define LAUNCH_START VALVE_OFF

//バルブ番号
typedef enum valve{
	VALVE1,
	VALVE2,
	VALVE3
}valve_t;

//一般フラグ処理用（要ヘッダ移動）
typedef enum orderstate{
	NO_ORDER,
	GOD_ORDER,
	WAIT_NEXT
}orderstate_t;

//バルブ汎用ステータス
typedef enum valvestate{
	VALVE_ON = GPIO_PIN_SET,
	VALVE_OFF = GPIO_PIN_RESET
}valvestate_t;

//サーボ割り当て
#define	SERVO_1 htim9
#define	SERVO_2 htim11
#define	SERVO_3 htim13

void undermotor_init(void);
void undermotor_reset(void);
uint8_t undermotor_raw(undermotor_t mchannel, float mspeed);
void undermotor_xy(float x_speed, float y_speed, float m_omega);

uint8_t undermotor_xy_next(float x_speed, float y_speed, float m_omega, float m_theta);

void Generalmotor_init(void);
void Generalmotor(uint32_t g_channel,float mspeed);

void Servomotor_init(void);
void Servomotor(TIM_HandleTypeDef *htims,uint32_t value);

void Valve(valve_t valve_num, valvestate_t valve_state);
void Valve_Toggle(valve_t valve_num);

#endif /* MOTOR_H_ */

/*
 * setgainparam.c
 *
 *  Created on: 2018/06/19
 *      Author: Ryohei
 */


#include "setgainparam.h"

struct_parameter parameter;

void SetParameter(void) {
	parameter.mass = 0.120f;
	parameter.motor_r = 0.0f;
	parameter.motor_km = 0.0f;
	parameter.motor_ke = 0.0f;
	parameter.gear_eff = 0.0f;
	parameter.wheel_radius = 0.127f;
	parameter.gear_ratio = 17;
}

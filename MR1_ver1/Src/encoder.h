/*
 * encoder.hpp
 *
 *  Created on: 2018/05/23
 *      Author: Ryohei
 */

#ifndef ENCODER_H_
#define ENCODER_H_
#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f7xx_hal.h"
#include "periph_handler.h"
#include "robotstate.h"
#include "setgainparam.h"
#include <math.h>

#define ENC_X_TIM_HANDLER		htim3
#define TIMx_ENC_X				TIM3
#define ENC_X_FW_CNTUP
//#define ENC_X_FW_COUTDOWN

#define ENC_Y_TIM_HANDLER		htim2
#define TIMx_ENC_Y				TIM2
#define ENC_Y_FW_CNTUP
//#define ENC_Y_FW_CNTDOWN

//リセット値
#define ENC_CNT_RESET			32768

//分解能(CPR)
#define ENC_RESOLUTION			4096.0f

//転がしオムニの直径
#define ENC_OMNI_RADIUS			0.060f

//Launcher
#define ENC_ANG_FW_CNTUP
#define ENC_ANG_HANDLER	htim4

//プロトタイプ宣言
void EncoderGetData(struct_state *);
void EncoderEnable(void);
void EncoderDisable(void);

void AngleEncoderEnable(void);
void AngleEncoderGet(int32_t *);


#ifdef __cplusplus
}
#endif
#endif /* ENCODER_H_ */

/*
 * user_interface.hpp
 *
 *  Created on: 2018/05/20
 *      Author: Ryohei
 */

#ifndef USER_INTERFACE_H_
#define USER_INTERFACE_H_
#ifdef __cplusplus
extern "C" {
#endif


#include "stm32f7xx_hal.h"
#include "periph_handler.h"

/*-----------------------------------------------
 * マイクロ秒タイマ
 ----------------------------------------------*/
/*
//タイマハンドラ
#define MICROSEC_TIM_HANDLER	htim10

//プロトタイプ宣言
void DelayMicroSec(uint32_t);
*/

/*-----------------------------------------------
 * LED・ブザー表示パターン
 ----------------------------------------------*/
//プロトタイプ宣言
void UIWarning(void);
void UIModeExec(void);
void UIModeBack(void);

/*-----------------------------------------------
 * UILED
 ----------------------------------------------*/
//UILEDのポート、ピン
#define UILED_BIT1_PORT		GPIOE
#define UILED_BIT1_PIN		GPIO_PIN_0

#define UILED_BIT2_PORT		GPIOG
#define UILED_BIT2_PIN		GPIO_PIN_8

#define UILED_BIT3_PORT		GPIOG
#define UILED_BIT3_PIN		GPIO_PIN_5

#define UILED_BIT4_PORT		GPIOG
#define UILED_BIT4_PIN		GPIO_PIN_6

//プロトタイプ宣言
void UILEDSet(uint8_t);


/*-----------------------------------------------
 * Buzzer
 ----------------------------------------------*/
//ブザータイマーハンドラ
#define BUZZER_TIM_HANDLER		htim14

//ブザータイマーチャンネル
#define BUZZER_TIM_CHANNEL		TIM_CHANNEL_1

//ブザータイマー周波数（分周後）
#define BUZZER_TIM_FREQ			1000000.0f

//音階
enum {
	BUZZER_A4 =  440,
	BUZZER_B4 =  494,
	BUZZER_C5 =  524,
	BUZZER_CM5 =  554,
	BUZZER_D5 =  588,
	BUZZER_E5 =  660,
	BUZZER_F5 =  699,
	BUZZER_FM5 =  740,
	BUZZER_G5 =  784,
	BUZZER_Am5 =  831,
	BUZZER_A5 =  880,
	BUZZER_AM5 =  932,
	BUZZER_B5 =  988,
	BUZZER_C6 = 1047,
	BUZZER_CM6 = 1109,
	BUZZER_D6 = 1175,
	BUZZER_Em6 = 1245,
	BUZZER_E6 = 1319,
	BUZZER_F6 = 1397,
	BUZZER_FM6 = 1480,
	BUZZER_G6 = 1568,
	BUZZER_GM6 = 1661,
	BUZZER_Am6 = 1661,
	BUZZER_A6 = 1760,
	BUZZER_Bm6 = 1865,
	BUZZER_B6 = 1976,
	BUZZER_C7 = 2093,
	BUZZER_D7 = 2349,
	BUZZER_E7 = 2637,
	BUZZER_F7 = 2794,
	BUZZER_G7 = 3136,
	BUZZER_A7 = 3520,
	BUZZER_B7 = 3951,
	BUZZER_C8 = 4186,
};

enum {
	ONE_2 = 1000,
	ONE_4 = ONE_2/2,
	ONE_8 = ONE_4/2,
	ONE_16 = ONE_8/2,
	ONE_32 = ONE_16/2
};

//プロトタイプ宣言
void BuzzerBeep(int16_t, int32_t);
void BuzzerOn(int16_t);
void BuzzerOff(void);
void Melody(uint8_t value);

/*-----------------------------------------------
 * Button
 ----------------------------------------------*/
//ON/OFF/長押し/短押し
enum {
	BUTTON_NUM_ERROR = -1,
	BUTTON_OFF = 0,
	BUTTON_ON,
	BUTTON_LONG,
	BUTTON_SHORT,
	BUTTON_PULL_UP,
	BUTTON_PULL_DOWN
};

//ボタンの個数
#define NUM_OF_BUTTON			3

//Buttonのポート、ピン
#if (NUM_OF_BUTTON > 0)
#define BUTTON_1_PORT			GPIOA
#define BUTTON_1_PIN			GPIO_PIN_15
//#define BUTTON_1_PULL_UP
#define BUTTON_1_PULL_DOWN
#endif

#if (NUM_OF_BUTTON > 1)
#define BUTTON_2_PORT			GPIOG
#define BUTTON_2_PIN			GPIO_PIN_7
//#define BUTTON_2_PULL_UP
#define BUTTON_2_PULL_DOWN
#endif

#if (NUM_OF_BUTTON > 2)
#define BUTTON_3_PORT			GPIOG
#define BUTTON_3_PIN			GPIO_PIN_4
//#define BUTTON_3_PULL_UP
#define BUTTON_3_PULL_DOWN
#endif

//プロトタイプ宣言
int8_t ButtonIsOn(uint8_t);
int8_t ButtonJudgePushTime(uint8_t, uint32_t);


#ifdef __cplusplus
}
#endif
#endif /* USER_INTERFACE_H_ */
